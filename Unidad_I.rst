.. header:: 
    Administración de GNU/Linux I - Unidad 1 - Introducción - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. role:: bash(code)
    :language: bash


.. Agregamos la Carátula definida en el estilo

.. Activamos el numerado de las secciones
.. sectnum::

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
    PageBreak


Edición 2022.

:Autor:       Leonardo Martinez  

.. 
    :Colaborador: 
    :Colaborador: 
    :Colaborador: 

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución Compartir Igual (CC-BY-SA) 4.0 Internacional. `<http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/licencia.png 

.. raw:: pdf

    PageBreak 
..    SetPageCounter 1 arabic


=============================================
Introducción a la Administración de GNU/Linux
=============================================

En esta materia el estudiante aprenderá los *conceptos básicos* necesarios para administrar una instalación de **GNU/Linux**, sea esta un equipo de uso personal o un servidor.

Las tareas de administración y gestión de los sistemas operativos del tipo **GNU/Linux** comúnmente se realizan desde la *línea de comando*. Sin embargo, también existen herramientas gráficas que permiten realizar estas tareas administrativas y de configuración. Estas herramientas gráficas son, generalmente, aplicaciones *front-end* que ejecutan en segundo plano los mismos comandos disponibles en la *línea de comando*.

Es probable que resulte un poco complicado y cueste acostumbrarse a utilizar la *consola* de un sistema operativo. Pero a medida que se toma experiencia, el trabajo se torna más simple y dinámico.

Muchas veces es necesario administrar un equipo remoto, que podría estar, no solo en la red doméstica o de trabajo, sino en otro edificio, en otra ciudad, en cualquier parte del mundo o incluso en un satélite o estación espacial. En estos casos el uso de la *línea de comando* es muy útil para aprovechar los accesos a redes con conexiones de datos lentas.

Otra utilidad muy importante y provechosa es la capacidad de creación de **shell scripts**, estos programas permiten la planificación y ejecución de tareas repetitivas o rutinarias de la administración de los sistemas y servicios que provee el sistema operativo GNU/Linux.

Muchas de las distribuciones de **GNU/Linux** se ajusta a la especificación *Linux Standard Base* [#]_ o simplemente **LSB**. De acuerdo a la definición de la propia **LSB**:

.. [#] http://www.linuxfoundation.org/collaborate/workgroups/lsb

.. class :: blockquote

El objetivo de la *LSB* es desarrollar y promover un conjunto de estándares que aumentarán la compatibilidad entre las distribuciones de *Linux* y permitirán que los programas de aplicación puedan ser ejecutados en cualquier sistema que se adhiera a ella. Además, la *LSB* ayudará a coordinar esfuerzos tendentes a reclutar productores y proveedores de programas que creen productos originales para Linux o adaptaciones de productos existentes.

Entre las tantas cosas que especifica la **LSB**: bibliotecas estándar, un conjunto de órdenes y utilidades que extienden el estándar POSIX, la estructura jerárquica del sistema de archivos, los niveles de ejecución, y varias extensiones al sistema gráfico X Window y Wayland.

Sin embargo, hay algunas distribuciones que vienen manteniendo críticas al respecto. Tal es el caso de **Debian** que retiró el soporte a la misma en Setiembre de 2015, manteniendo como única compatibilidad la estructura jerárquica de archivos.

Los **comandos** o instrucciones, que se verán en esta materia son comunes a todos los sistemas **GNU/Linux** puesto que forman parte del estándar **POSIX**. Cada distribución utilizan sus propias aplicaciones para algunas tareas de administración y gestión, como la que administra el software que está disponible en sus repositorios.

Con estos comandos se pueden administrar casi todos los aspectos del sistema operativo, desplazarse por sus directorios y archivos sin importar qué distribución de **GNU/Linux** se esté utilizando.

La mayoría de los comandos suele tener una variedad de parámetros que permiten realizar tareas más específicas de acuerdo a las necesidades de uso, como copiar o mover archivos manteniendo los permisos o listar los archivos y directorios con algún formato de visualización particular.

Se trabajará con los comandos y sus parámetros más comunes y de mayor uso. La documentación existente en la ayuda de cada comando es lo suficientemente clara y explicativa para que el alumno pueda entenderlos y realizar las pruebas que requiera para su aprendizaje.

Rol del Administrador de Sistemas
---------------------------------

Las grandes empresas y organizaciones dependen cada vez más de sus recursos de informáticos y de cómo estos son administrados para adecuarlos a las tareas. El gran incremento de las redes distribuidas, con sus equipos servidores y clientes, ha creado una gran demanda de un nuevo perfil laboral: el **administrador de sistemas**.

El **administrador de sistemas** tiene una amplia variedad de tareas importantes. Los mejores administradores de sistema suelen ser bastante generalistas (aunque el mercado y los perfiles profesionales acaban imponiendo una especialización importante), tanto teórica como prácticamente. El **administrador de sistemas** es el encargado de manter y optimizar el uso de los recursos tecnológicos de los cuales es responsable.

Existen distintos tipos de tareas de soporte técnico y administración, y se pueden clasificar según su complejidad, su frecuencia y el nivel de seguridad.
En grandes estructuras organizativas el *rol* del **administrador de sistemas** puede necesitar el trabajo de un equipo de personas que cumplan con ese rol, distribuyéndose las tareas y responsabilidades.
En el casos de pequeñas y medianas empresas ese *rol* es muchas veces realizado por una única persona. 

En lo que a la *Administración de GNU/Linux* se refiere, el **administrador de sistemas** es el responsable de asegurar que la instalación sea consistente y pueda brindar y mantener los servicios necesarios para que los usuarios puedan desarrollar sus actividades asociadas al funcionamiento de la institución o empresa.

EL trabajo consiste en desarrollar y gestionar herramientas preventivas que, en una evolución constante, generen un estado de calidad y seguridad en el servicio tecnológico. Existe en la *cultura sysadmin* un par de frases que, con mucho humor, explican esta idea de **trabajar para tener menos trabajo**:

.. class :: blockquote

"Si el administrador de sistemas tiene mucho tiempo ocioso es porque está haciendo bien su trabajo"

.. class :: blockquote

"Si el administrador de sistemas mira la pantalla con preocupación, preocúpese usted también"


El administrador del sistema es responsable de asegurar que el sistema **GNU/Linux** brinde los servicios necesarios para que los usuarios puedan cumplir con sus tareas. Esto involucra una serie de actividades de suma importancia:

* Agregar nuevos usuarios al sistema y configurar sus directorios personales y los privilegios básicos.

* Instalar software, incluyendo aplicaciones, nuevas versiones del sistema y sus aplicaciones y corregir errores.

* Monitorear el uso del sistema de archivos, verificar que los usuarios uilicen adecuadamente los recursos y que las políticas de seguridad y backups están siendo implementadas.

* Formar y asistir a los usuarios y atender los reportes de problemas presentados por estos.

* Instalar nuevos componentes de hardware y verificar la correcta instalación de los módulos asociados a éstos.

* Asegurarse de que los servicios básicos de infraestructura estén disponibles, tales como servicio de archivos, servicio de impresión, servicio de correo electrónico, servicio DNS, Servicio de Autenticación por mencionar algunos.

* Realizar cableados de instalaciones o reparar cables.

* Desarrollar trucos o técnicas para mejorar la productividad.

* Evaluar económicamente compras de equipamiento de hardware y software.

* Automatizar un gran número de tareas comunes, e incrementar el rendimiento general del trabajo en su organización.


==============================
Preparar el entorno de trabajo
==============================

El primer paso en la administración de un sistema **GNU/Linux** es el *proceso de instalación*. Si bien ya se ha visto este proceso en la materia **Ofimática en las organizaciones**, desde el punto de vista de la administración, se debe conocer en detalle cada una de las etapas del proceso.

La mayoría de las distribuciones de **GNU/Linux** tienen un proceso instalación muy bien elaborado e intuitivo, basta con tener definido que tipo de servicios va a prestar el equipo y las necesidades del usuario que va a utilizar el equipo.

Siguiendo el mismo camino visto en las otras materias, se utilizará la distribución **Xubuntu** [#]_ en su versión *LTS (Long Term Support)* más reciente (A la fecha de creación de este documento es la *22.04* publicada el 21 de abril de 2022).

.. [#] http://xubuntu.org/

.. raw:: pdf

    PageBreak

Descargar el archivo ISO
------------------------

Desde el sitio de descarga [#]_ se descarga la versión de *64 bits*.

.. [#] http://xubuntu.org/getxubuntu/



Preparar el disco de instalación
++++++++++++++++++++++++++++++++

El tamaño de las imagenes *ISO* actuales supera el tamaño de un CD, por lo tanto se debe grabar en un DVD. Para ello se puede utilizar cualquier aplicación de grabación en cualquiera de los sistemas operativos disponibles.


Instalar el sistema operativo
-----------------------------

El proceso de instalación que se llevará adelante durante el ejemplo, se hará sobre una máquina virtual en el entorno de *VirtualBox* (Se trabaja en la versión 7.0.2). A continuación se muestra el proceso de instalación para obtener un entorno de trabajo para desarrollar las actividades de esta materia.

Configuración de la máquina virtual
+++++++++++++++++++++++++++++++++++

Iniciar la aplicación *VirtualBox* y presionar el botón *Nueva*.

.. image:: imagenes/VB_01.png
   :scale: 130

.. raw:: pdf

    PageBreak

Ingresar el nombre de la VM. Seleccionar la imagen ISO descargada. Marcar la casilla **Skip Unattended Installation**. Presionar el botón *Next*.

.. image:: imagenes/VB_02.png
   :scale: 200

La configuración por defecto de memoria y CPU es suficiente. Presionar el botón *Next*.

.. image:: imagenes/VB_03.png
   :scale: 200

.. raw:: pdf

    PageBreak

La configuración por defecto de disco virtual es suficiente. Presionar el botón *Next*.

.. image:: imagenes/VB_04.png
   :scale: 200

En la presentación del resumen verificar que esté de acuerdo a lo configurado. Presionar el botón *Terminar*.

.. image:: imagenes/VB_05.png
   :scale: 200

.. raw:: pdf

    PageBreak

En la vista de configuración de la VM, ingresar a la sección **Pantalla**.

.. image:: imagenes/VB_06.png
   :scale: 130

Configurar la *Memoria de video* al máximo (128Mb). Presionar el botón *Next*.

.. image:: imagenes/VB_07.png
   :scale: 200

.. raw:: pdf

    PageBreak

En la vista de configuración de la VM, presionar el botón *Iniciar*. Se abrirá otra ventana con la visualización de la VM corriendo. Seleccionar la opción *Try or Install Xubuntu*.

.. image:: imagenes/VB_08.png
   :scale: 230

Seleccionar el idioma *Español* y luego la opción *Instalar Xubuntu*.

.. image:: imagenes/VB_09.png
   :scale: 170

.. raw:: pdf

    PageBreak

Seleccionar la distribución de teclado acorde al modelo que se está utilizando. Presionar el botón *Continuar*.

.. image:: imagenes/VB_10.png
   :scale: 170

Marcar la opción *Instalar programas de terceros para hardware de gráficos y de wifi y formatos multimedia adicionales*. Presionar el botón *Continuar*.

.. image:: imagenes/VB_11.png
   :scale: 170

.. raw:: pdf

    PageBreak

Presionar el botón *Continuar*.

.. image:: imagenes/VB_12.png
   :scale: 170

Presionar el botón *Continuar*.

.. image:: imagenes/VB_13.png
   :scale: 200

.. raw:: pdf

    PageBreak

Seleccionar en el mapa la ciudad de *Buenos Aires*. Presionar el botón *Continuar*.

.. image:: imagenes/VB_14.png
   :scale: 170

Completar el formulario con los datos solicitados con la información establecida. Presionar el botón *Continuar*.

.. image:: imagenes/VB_15.png
   :scale: 170

.. raw:: pdf

    PageBreak

Presionar el botón *Reiniciar ahora*.

.. image:: imagenes/VB_16.png
   :scale: 170

Una vez iniciada la VM con el sistema instalado se presenta la pantalla de inicio de sesión.

.. image:: imagenes/VB_17.png
   :scale: 170




sudo apt install build-essential dkms


Definiciones de la instalación
++++++++++++++++++++++++++++++

Siguiendo la instalación del video se podrá observar una serie de parámetros de configuración que se van aplicando en cada paso del proceso.

A diferencia de una instalación tradicional, en la que se seleccionan las opciones por defecto que presenta el asistente de instalación en cada etapa del proceso, en este caso hay dos cuestiones a tener en cuenta, la primera es el **particionamiento del disco** y la segunda es la creación del usuario **Administrador**. El por qué de estas definiciones se analizan con más detalle en la *Unidad II*.

.. raw:: pdf

    Spacer 0 10

.. figure:: imagenes/particiones_disco_rigido.png
   :width: 15cm

   Particionamiento del disco rígido.


.. raw:: pdf

    PageBreak

==================
Conocer el entorno
==================

Los sistemas *GNU/Linux* son sistemas multitarea, ejecutan muchas tareas al mismo tiempo independientemente de la cantidad de *cores* o *CPU* de la computadora mediante el uso de la multiprogramación, una característica común en todos los sistemas operativos considerados modernos. Con esta funcionalidad una tarea se ejecuta durante un determinado tiempo, luego se suspende, pasa a la siguiente y así sucesivamente, y cuando se llega al final, vuelve a comenzar por la primera sin afectar su comportamiento o ejecución.

Este tipo ejecución, que se denomina **round robin**, distribuye un *momento* o *quantum* de tiempo, de entre 15 milisegundos a 150 milisegundos, para cada tarea en espera y volviendo a comenzar el ciclo con la primera cuando se termina la cola de procesos. En el caso de computadoras que tengan más de un *core* o *CPU*, los sistemas *GNU/Linux* son capaces de repartir las tareas entre las unidades de proceso logrando un mejor rendimiento.

Entorno gráfico y consola
-------------------------

En los sistemas *GNU/Linux* se puede trabajar en dos entornos diferentes, por un lado el entorno gráfico que permite una interacción más cómoda y agradable para la realización de las tareas cotidianas y de trabajo que tiene un usuario hogareño o de oficina.

El otro método de trabajo, menos visual pero no menos efectivo y versátil, se utiliza generalmente para realizar tareas de adiministración del sistema operativo y tareas más concretas de mantenimiento, respaldo y restauración de archivos.

Este último método, de consola o terminal de texto, es utilizado en mayor medidad en los equipos informáticos que cumplen funciones de servidor, sin embargo hay muchas tareas de adiministración de estaciones de trabajo que se pueden realizar remotamente y el entorno de la consola es el ideal, permitendo, en lo posible, operar sobre el equipo sin necesidad de molestar o interrumpir al usuario que está realizando sus tareas.

Se puede utilizar la consola de texto ejecutando la aplicación *Terminal* dentro del entorno gráfico o simplemente accediendo a una de las consolas disponibles en el sistema que no están corriendo el sistema gráfico.

En la mayoría de los sistemas *GNU/Linux*, se dispone de siete terminales, de la **1** a la **6** para terminales de texto y la **7** corresponde al terminal donde se ejecuta el entorno gráfico.

Para acceder a las distintas terminales se utiliza la combinación de teclas **<CTRL>+<ALT>+<Fn>**, donde la tecla de función *1* a *7* corresponden a las terminales descriptas.

Todas las terminales son concurrentes, por lo tanto, sus respectivas sesiones se ejecutan en forma simultánea compartiendo los recursos del equipo gracias a la característica multitarea del sistema operativo.

El intérprete de comandos
+++++++++++++++++++++++++

Este intérprete de órdenes o comandos se encarga de la interacción entre el usuario y el sistema operativo. El nombre más utilizado para referirse al intérprete es **shell**, y existe una interesante variedad a disposición del usuario. El **shell** por defecto instalado en la mayoría de las distribuciones de *GNU/Linux* es el **Bash**, aunque existen otras alternativas más interesantes como **zsh** [#]_ y **fish** [#]_.

.. [#] http://www.zsh.org/
.. [#] https://fishshell.com/

El **shell** es lo primero que se tiene disponible cuando se accede a la consola de texto o terminal. Permite ejecutar comandos en forma interactiva obteniendo el resultado tras cada ejecución.

Una forma de automatizar tareas es la de escribir programas que el intérprete resolverá ejecutando los comandos incluidos en el mismo, siguiendo la lógica descripta. Estos programas se llaman **Shell Scripts** y consisten en archivos de texto con la secuencia de comandos a ejecutar definidos en el lenguaje específico del intérprete.

Al utilizar *shell scripts* se puede mejorar la lógica de la tarea a ejecutar mediante el uso de variables y estructuras de control, logrando en algunos casos desarrollos realmente complejos.

Sistema de archivos
-------------------

Los sistemas de archivos son estructuras de datos colocados dentro de una partición (un segmento de espacio en un dispositivo físico asignado para ser usado por un sistema de archivos).

Los sistemas Unix en general, y por lo tanto también *GNU/Linux*, presentan una estructura de archivos estándar que, a diferencia de otros sistemas operativos, considera a todos los componentes un archivo. El *kernel* es un conjunto de archivos, las librerías son archivos, el directorio es un archivo y los dispositivos son archivos. Estos últimos, por esta misma condición, no necesitan una forma particular de identificarlos como es el caso de *DOS* y *Windows*, que utilizan una letra.

Esta versatilidad de direccionar todo en una jerarquía de archivos permite incorporar a la estructura de archivos dispositivos de almacenamiento removibles e incluso recursos de archivos remotos en otros sistemas.

La jerarquía del sistema de archivos GNU/Linux es similar a la estructura de la raíz de un árbol. Comienza con un directorio particular denominado *directorio root* o *directorio raíz* y se ramifica en una esctructura definida.

El *sistema de archivos* está caracterizado por tres definiciones generales:

* Estructura jerárquica.

* Tratamiento consistente de la información de los archivos.

* Protección de los archivos.

Usuarios y grupos
-----------------

Otra característica importante de los sistemas Gnu/Linux es que son sistemas operativos multiusuario, esto permite que más de un usuario pueda realizar tareas con el sistema al mismo tiempo, logrando además que los trabajos concurrentes no interfieran unos con otros en el uso de los recursos.

Los sistemas *GNU/Linux*, al igual que todos los sistemas del tipo *Unix*, tienen identificados dos tipos o categorías de usuarios, diferenciadas por el tipo de función y roles dentro del sistema.

El usuario que tiene todos los permisos sobre el sistema es denominado el **Super Usuario**, generalmente identificado como el usuario local **root**. Los usuarios restantes con el rol de **Usuario Estándar**, tienen un espacio dedicado en el directorio :bash:`/home/<nombre_usuario>` dentro del cual pueden gestionar su espacio de trabajo con total libertad. Estos espacios individuales son exclusivos de cada usuario y cuyos permisos no permiten el acceso por parte de otros usuarios del sistema, con la única excepción del usuario **root**.

Fuera del directorio personal dentro de :bash:`/home` los usuarios pueden realizar tareas simples en el resto del sistema de archivos de *GNU/Linux*, en algunos casos sólo de lectura y ejecución. La gestión de acceso a los archivos para realizar estas tareas se formaliza mediante el otorgamiento de permisos sobre archivos y directorios.

Grupos
++++++

Los grupos de usuarios son conjuntos de usuarios, se utilizan para organizar los usuarios de acuerdo a las funciones que cumplen dentro del sistema. Simpifica la gestión de permisos de acceso a aplicaciones, archivos, directorios y dispositivos definidos en el sistema operativo.

Como todos los usuarios deben pertenecer, al menos, a un grupo, algunas distribuciones crean un grupop unipersonal con el mismo nombre del usuario y otras optan por utilizar un grupo de usuarios básico. La inclusión del usuario en uno o más grupos dependerá de los servicios del sistema que éste necesita utilizar para su tarea diaria.

