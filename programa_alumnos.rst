.. header:: 
    Administración de GNU/Linux I - Programa - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. role:: bash(code)
    :language: bash


.. Agregamos la Carátula definida en el estilo

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
    PageBreak


Copyright © 2016.

:Autor:       Leonardo Martinez  

.. 
    :Colaborador: 
    :Colaborador: 
    :Colaborador: 

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución Compartir Igual (CC-BY-SA) 4.0 Internacional. `<http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/licencia.png 

.. raw:: pdf

    PageBreak 

=============================
Administración de GNU/Linux I
=============================

Esta asignatura pretende acercar al alumno al ámbito de trabajo del *administrador de sistemas*, incorporando las bases y conceptos iniciales necesarios para llevar adelante dicho rol.

La materia se desarrolla distribuyendo los temas en *Unidades* que se verán en el transcurso de **10 semanas**. Esta organización temática permitirá una mejor comprensión de los conceptos.

En cada *Unidad* se proponen actividades a desarrollar para volcar los conceptos adquiridos. Las actividades enunciadas en el programa como **Trabajo Práctico** serán tomadas en cuenta para la regularización de la materia.

Para aprobar la materia, el alumno deberá rendir el exámen final de acuerdo a la reglamentación de la carrera en cuanto a correlatividades. El exámen podrá contar con una parte escrita y alguna parte práctica a desarrollar en el equipamiento dispuesto a tal fin.

=======
Docente
=======

**Leonardo Martinez**, es Técnico Superior en Tecnologías de la Información. Actualmente se desempeña en el Área de Ingeniería de la Sectorial Informática del Ministerio de Economía de la Provincia de Santa Fe donde realiza tareas de investigación, análisis e implementación de servicios de TICs. Es miembro del *LUGLi* ( `Grupo de Usuarios de Software Libre del Litoral <http://www.lugli.org.ar>`_ ) desde sus inicios.


=========
Contenido
=========

UNIDAD I - Introducción
-----------------------

**Fecha de Inicio**

*Lunes 13 de junio de 2016*

**Temas**

- Descripción de la materia y los contenidos presentados durante el cursado.
- Instalación de la máquina virtual que se usará durante la materia.
- Desarrollo de conceptos básicos.

.. raw:: pdf

    Spacer 0 10


**Actividades propuestas**

- Cuestionario.
- *Trabajo Práctico*

.. raw:: pdf

    Spacer 0 10

**Tiempo de desarrollo**

- 1 semana.


UNIDAD II - El sistema operativo
--------------------------------

**Fecha de Inicio**

*Lunes 20 de junio de 2016*

**Temas**

- Particiones.
- Sistema de archivos.
- Gestión de procesos.
- Comandos básicos.

.. raw:: pdf

    Spacer 0 10

**Actividades propuestas**

- Cuestionario.
- *Trabajo Práctico*

.. raw:: pdf

    Spacer 0 10

**Tiempo de desarrollo**

- 2 semanas


UNIDAD III - Usuarios y grupos
------------------------------

**Fecha de Inicio**

*Lunes 27 de junio de 2016*

**Temas**

- Tipos de usuarios.
- Administración de usuarios.
- Tipos de grupos.
- Administración de grupos.

.. raw:: pdf

    Spacer 0 10

**Actividades propuestas**

- Cuestionario.
- *Trabajo Práctico*

.. raw:: pdf

    Spacer 0 10

**Tiempo de desarrollo**

- 1 semana.


UNIDAD IV - Gestión de software
-------------------------------

**Fecha de Inicio**

*Lunes 04 de julio de 2016*

**Temas**

- Gestión de repositorios.
- Actualización de paquetes y aplicaciones.
- Instalación de paquetes y aplicaciones.

.. raw:: pdf

    Spacer 0 10

**Actividades propuestas**

- Cuestionario.
- *Trabajo Práctico*

.. raw:: pdf

    Spacer 0 10

**Tiempo de desarrollo**

- 1 semana.


UNIDAD V - Trabajar desde la línea de comando
---------------------------------------------

**Fecha de Inicio**

*Lunes 11 de julio de 2016*

**Temas**

- Operación desde la consola.
- Utilización de filtros y tuberías.
- Introducción a la programación de *shell scripts*.

.. raw:: pdf

    Spacer 0 10

**Actividades propuestas**

- Cuestionario.
- *Trabajo Práctico*

.. raw:: pdf

    Spacer 0 10

**Tiempo de desarrollo**

- 2 semanas


UNIDAD VI - Compartir recursos locales y externos
-------------------------------------------------

**Fecha de Inicio**

*Lunes 25 de julio de 2016*

**Temas**

- Compartir recursos locales.
- Utilizar recursos compartidos externos.
- Montaje de recurso, manual y automático.

.. raw:: pdf

    Spacer 0 10

**Actividades propuestas**

- Cuestionario.
- *Trabajo Práctico*

.. raw:: pdf

    Spacer 0 10

**Tiempo de desarrollo**

- 2 semanas

.. raw:: pdf

    PageBreak


UNIDAD VII - Configuración de dispositivos
------------------------------------------

**Fecha de Inicio**

*Lunes 08 de agosto de 2016*

**Temas**

- Tipos de dispositivos.
- Configuración de impresoras.
- Configuración de otros dispositivos.

.. raw:: pdf

    Spacer 0 10

**Actividades propuestas**

- Cuestionario.
- *Trabajo Práctico*

.. raw:: pdf

    Spacer 0 10

**Tiempo de desarrollo**

- 1 semana.


