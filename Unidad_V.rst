.. header:: 
    Administración de GNU/Linux I - Unidad 5 - Línea de comando y Scripting - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. role:: bash(code)
    :language: bash

.. role:: perl(code)
    :language: perl

.. Agregamos la Carátula definida en el estilo

.. Activamos el numerado de las secciones
.. sectnum::

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
    PageBreak


Copyright © 2016.

:Autor:       Leonardo Martinez  

.. 
    :Colaborador: 
    :Colaborador: 
    :Colaborador: 

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución Compartir Igual (CC-BY-SA) 4.0 Internacional. `<http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/licencia.png 

.. raw:: pdf

    PageBreak 
..    SetPageCounter 1 arabic


Introducción a la consola
=========================

A simple vista un *shell* es un intérprete de comandos. Normalmente es ejecutado en un modo interactivo donde el usuario escribe comandos, y el *shell* provee salidas de texto a esos comandos. La mayoría de los sistemas operativos poseen *shells*; hasta el *MS-DOS* provee un *shell* simple de nombre **command.com**. En ocasiones el *shell* también es llamado el *Intérprete de la Línea de Comandos* (CLI - Command Line Interpreter).

El intérprete de comandos, el *shell*, tiene una importancia fundamental en las tareas del administrador de sistemas brindando la posibilidad de ejecutar archivos de comandos secuenciales escritos en texto plano y que éste los interpretará. Estos archivos se denominan *shell scripts* y son una herramienta imprescindible en el día a día de los administradores de sistemas.
 
La práctica de *shell scripting* es necesaria ya que las distrbuciones *GNU/Linux* se basan en este tipo de recurso para iniciar el sistema ejecutando los archivos de :bash:`/etc/rcS.d`. Los administradores deben tener los conocimientos necesarios para trabajar con ellos, entender el funcionamiento del sistema, modificarlos y adecuarlos a las necesidades específicas del entorno en el cual trabajen.

Se considera el *scripting* casi un arte, sin embargo no es difícil de aprender, se pueden aplicar técnicas de trabajo por etapas que se ejecutará en forma secuencial y el conjunto de operadores u opciones no es tan extenso como para que genere dudas sobre qué recurso utilizar. La sintaxis es dependiente del *shell* utilizado, pero al ser un lenguaje interpretado con encadenamiento de secuencias de comandos, es muy fácil de depurar y poner en funcionamiento.

Entre los lenguajes interpretados que han popularizado la técnica del *scripting* están **Perl**, **AWK**, **sed**, **Lisp**, **PHP**, **Python** y **Ruby**. Algunos incluyen características como el soporte para orientación a objetos, programación imperativa o funcional.

La mayoría de las distribuciones *GNU/Linux* utilizan como intérprete de comandos en la consola el *GNU Bourne-Again SHell* conocido como :bash:`bash`, sin embargo se pueden utilizar otros intérprete que incorporan nuevas características o mejoras como el caso de :bash:`zsh` [#]_ o :bash:`fish` [#]_.

.. [#] `<http://www.zsh.org/>`_

.. [#] `<https://fishshell.com/>`_


El *shell* de *GNU/Linux*
-------------------------

El objetivo principal del shell es invocar o *lanzar* otro programa, sin embargo, suelen tener capacidades adicionales, tales como ver el contenido de los directorios, interpretar órdenes condicionales, trabajar con variables internas, gestionar interrupciones, redirigir entrada/salida, etc.

    El shell es una pieza de software que proporciona una interfaz para los usuarios en un sistema operativo y que provee acceso a los servicios del núcleo. Su nombre proviene de la envoltura externa de algunos moluscos, ya que es la "parte externa que protege al núcleo".

Entre los shells más populares (o históricos) en los sistemas **\*nix** tenemos:

* **Bourne shell** (sh)
* **Almquist shell** (ash) o su versión en *Debian* (dash)
* **Bourne-Again shell** (bash)
* **Korn shell** (ksh)
* **Z shell** (zsh)
* **C shell** (csh) o la versión **Tenex C shell** (tcsh)

El *Bourne shell* ha sido el estándar *de facto* ya que fue distribuido en *Unix Version 7* publicado en 1977 y *Bourne-Again Shell (Bash)* es una versión mejorada del primero escrita por el proyecto **GNU** bajo *licencia GPL*, la cual se ha transformado en el estándar de los sistemas *GNU/Linux*.

Todos los ejemplos de esta unidad están basados en el uso de :bash:`bash`. Sin embargo es posible instalar cualquiera de los disponibles y utilizarlo. Para saber cuáles son los intérpretes disponibles en el sistemas se debe ejecutar el comando:

.. code-block:: bash

    $ cat /etc/shells 
    # /etc/shells: valid login shells
    /bin/sh
    /bin/dash
    /bin/bash
    /bin/rbash

    $ 

.. raw:: pdf

    Spacer 0 5

El *shell* por defecto para un usuario se indica en el último campo de la línea correspondiente al usuario en el fichero :bash:`/etc/passwd`.

.. code-block:: bash

    aswartz:x:1005:1006:Aaron Swartz,,,,Alumno:/home/aswartz:/bin/bash

.. raw:: pdf

    Spacer 0 5

Para conocer el *shell* activo se debe ejecutar el comando:

.. code-block:: bash

    $ echo $SHELL
    /bin/bash
    $ 
.. raw:: pdf

    Spacer 0 5

Se puede utilizar cualquiera de los *shells* disponibles en el sistema ejecutando el binario correspondiente. Al salir, se vuelve al *shell* inicial. Para cambiar definitivamente el *shell* de usuario se debe actualizar el archivo :bash:`/etc/passwd` con el comando:

.. code-block:: bash

    $ chsh -s /bin/dash
    Contraseña: 
    $ 

Al iniciar una nueva consola se verifica que el nuevo *shell* es efectivo:

.. code-block:: bash

    $ echo $SHELL
    /bin/dash
    $ 

Cada usuario sólo puede cambiar su propio *shell* y los usuarios administradores pueden cambiar el de todos los usuarios.


Comandos internos del shell
+++++++++++++++++++++++++++

Existen un conjunto de comandos internos [#]_ al *shell*, es decir, integrados con el código de éste que, en el caso *Bourne* son:

.. raw:: pdf

    Spacer 0 5

.. code-block:: bash

    :, ., break, cd, continue, eval, exec, exit, export, getopts, hash, pwd, readonly,
    return, set, shift, test, [, times, trap, umask, unset.

.. raw:: pdf

    Spacer 0 5

Y además Bash incluye:

.. raw:: pdf

    Spacer 0 5

.. code-block:: bash

    alias, bind, builtin, command, declare, echo, enable, help, let, local, logout,
    printf, read, shopt, type, typeset, ulimit, unalias.

.. raw:: pdf

    Spacer 0 5

Cuando el intérprete *Bash* ejecuta un *script shell*, crea un proceso hijo que ejecuta otro *Bash*, el cual lee las líneas del archivo de a una línea por vez, las interpreta y ejecuta como si los comandos vinieran del teclado uno a uno.

El proceso *Bash* padre espera mientras el proceso *Bash* hijo ejecuta el *shell script* hasta el final, cuando el control vuelve al proceso padre, éste vuelve a poner el *prompt* nuevamente.

Un comando de *shell* será algo tan simple como :bash:`touch archivo1 archivo2 archivo3` que consiste en el propio comando seguido de argumentos, separados por espacios considerando :bash:`archivo1` el primer argumento y así sucesivamente.

.. [#] Consultar la página de manual para más información y descripción más completa.


Variables de usuario y de entorno
+++++++++++++++++++++++++++++++++

Existen dos tipos de variables, la que puede crear el usuario y asginarle un valor y las de entorno que ya están creadas al momento de iniciar el sistema y/o el *shell*, cuyo valor puede ser cambiado por el usuario.

Variables de usuario
~~~~~~~~~~~~~~~~~~~~

Las variables de usuario son usadas mayormente dentro de los *scripts*, le otorgan una potencia importante al momento de manipular la información de contexto para tomar decisiones durante la ejecución de los *scripts*.

Los nombres de las variables consisten en letras, dígitos y guiones y se crean nombrándolas y asignándoles un valor en una sentencia del tipo :bash:`nombre_de_variable=valor`.

La variable existe mientras el proceso de *shell* en el que se creó esté activo, mantiene su espacio en memoria siempre y cuando su *shell* padre exista, no se pueden eliminar asignándole el valor :bash:`null`.


El alcance de una variable se refiere a la disponibilidad de la misma para otros procesos. Por defecto, las variables sólo son efectivas en el proceso que son definidas o sus procesos hijos. Así, una variable definida en un *shell script* es visible para los procesos hijos iniciados en el mismo y se denominan variables *privadas* o *locales*. 

Para que una variable sea considerada *global* y esté disponibles para todos los procesos del *shell* se debe hacer explícita la definición mediante el comando :bash:`export`.

.. code-block:: bash

    $ export directorio=/home/administrador/pruebas
    $

Para hacer referencia a una variable se debe anteponer al nombre el caracter :bash:`$`, por ejemplo par ver el contenido de una variable se utiliza el comando :bash:`echo`:

.. code-block:: bash

    $ echo $directorio
    /home/administrador/pruebas
    $


Variables de entorno
~~~~~~~~~~~~~~~~~~~~

El funcionamiento y aspecto del *shell* son el resultado de las variables de entorno almacenadas en los archivos de configuración del sistema. Algunos de estos archivos son de configuración general como :bash:`/etc/profile` y :bash:`/etc/bash.bashrc`, y otros son personales de cada usuario, como :bash:`.profile` y :bash:`.bashrc`, alojados en el directorio :bash:`$HOME`

El listado de las variables de entorno definidas en el *shell* se pueden obtener indistintamente con dos comandos, :bash:`printenv` y :bash:`env`.

.. code-block:: bash

    $ env
    XDG_SESSION_ID=4
    TERM=xterm
    SHELL=/bin/bash
    SSH_CLIENT=192.168.0.106 54641 22
    SSH_TTY=/dev/pts/4
    USER=administrador
    LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd
    =40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;4
    4:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;
    31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7
    z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=0
    1;31:*.lzo=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz
    =01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*
    .rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;3
    1:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm
    =01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:
    *.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=0
    1;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*
    .m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;
    35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=
    01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.o
    gv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36
    :*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=0
    0;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:
    MAIL=/var/mail/administrador
    PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/us
    r/local/games:/snap/bin
    PWD=/home/administrador
    LANG=es_AR.UTF-8
    SHLVL=1
    HOME=/home/administrador
    LANGUAGE=es_AR:es
    LOGNAME=administrador
    SSH_CONNECTION=192.168.0.106 54641 192.168.0.100 22
    LC_CTYPE=es_AR.utf8
    LESSOPEN=| /usr/bin/lesspipe %s
    XDG_RUNTIME_DIR=/run/user/1000
    LESSCLOSE=/usr/bin/lesspipe %s %s
    _=/usr/bin/env
    
    $ 

La diferencia entre ambos comandos es que en el caso de :bash:`printenv` se puede utilizar para conocer el valor de una variable de entorno particular:

.. code-block:: bash

    $ printenv SHELL
    /bin/bash
    $ printenv USER
    administrador
    $ 

El comando :bash:`env` permite modificar el entorno en el que corren los programas pasándole un grupo de definiciones de variables en una línea de comando, por ejemplo:

.. code-block:: bash

    $ env HOME="/usr/local/programa" comando_a_ejecutar opciones_comando

Como se indica antes, los procesos hijos heredan las variables de entorno del proceso padre, al usar :bash:`env` de esta manera, otorga la posibilidad de reemplazar o agregar nuevos valores al entorno del proceso hijo.

Para ver las variables del *shell* activo el comando indicado es :bash:`set` que, al ejecutarse sin parámetros muestra las variables configuradas y las funciones *bash* definidas. Normalmente la extensión de la salida de este comando es muy grande.

Variables comunes
~~~~~~~~~~~~~~~~~
Algunas de las variables de entorno y del *shell* se utilizan con mucha frecuencia. Algunas de ellas son:


SHELL
    Describe el *shell* activo que interpreta los comandos, en la mayoría de los casos será :bash:`bash`.

.. raw:: pdf

    Spacer 0 5

TERM
    Especifica el tipo de terminal que se emula cuando corre el *shell*.

.. raw:: pdf

    Spacer 0 5

USER
    El usuario en actualmente en sesión.

.. raw:: pdf

    Spacer 0 5

PWD
    El directorio de trabajo actual.

.. raw:: pdf

    Spacer 0 5

OLDPWD
    El directorio de trabajo anterior. El *shell* mantiene la información para poder regrear al directorio anterior con el comando: :bash:`cd -`.

.. raw:: pdf

    Spacer 0 5

MAIL
    La ubicación del buzón de correo del usuario actual.

.. raw:: pdf

    Spacer 0 5

PATH
    Un listado de directorios que el sistema utilizará para buscar los comandos.

.. raw:: pdf

    Spacer 0 5

LANG
    El lenguaje y configuración de localización actual, incluyendo la codificación de caracteres.

.. raw:: pdf

    Spacer 0 5

HOME
    El directorio de inicio del usuario actual.

.. raw:: pdf

    Spacer 0 5

\_
    El último comando ejecutado.

.. raw:: pdf

    Spacer 0 10

Además de estas variables de entorno, también están algunas variables del *shell* muy útiles:

.. raw:: pdf

    Spacer 0 10

BASH_VERSION
    La versión de :bash:`bash` que se está ejecutando en formato legible por un humano.

BASH_VERSINFO
    La versión de :bash:`bash` en formato válido para leer con máquina.

HOSTNAME
    El nombre de **host** de la computadora.

IFS
    Siglas de *Internal Field Separator* para separar la entrada en la línea de comando. Por defecto es un espacio en blanco.

UID
    El *UID* del usuario actual.


.. raw:: pdf

    PageBreak

Redireccionamiento y tuberías
-----------------------------

En los sistemas **\*nix** cada proceso puede tener tres descriptores de archivos del estándar *POSIX* que, a modo de canales, conectan la entrada y salida *(I/O)* de un comando o aplicación con la terminal o consola cuando se ejecuta:

* Entrada estándar **stdin** (standard input)
* Salida estándar **stdout** (standard output)
* Error estándar **stderr** (standard error)

Estos canales se utilizan para enviar y recoger los datos que surgen de la ejecución de un *script*, comando o aplicación y se representan con un número entero, el número :bash:`0` representa a **stdin**, el :bash:`1` a **stdout**, y el :bash:`2` a **stderr**.

En la mayoría de los *shells*, incluso en *Bash*, se puede redirigir **stdout** y **stderr** a un archivo.

Por ejemplo, para enviar la salida estándar del comando :bash:`ls` a un archivo el comando es:

.. code-block:: bash

    $ ls -l > directorio.txt

Este comando creará un archivo llamado :bash:`directorio.txt`, cuyo contenido será lo que se vería en la pantalla si se ejecutara el comando :bash:`ls -l` solamente.


Entrada estándar (stdin)
++++++++++++++++++++++++

El canal **stdin** o *standard input* dirige los datos que son enviados al programa, puede ser el usuario de terminal o la salida de datos de un programa.

En un script en **perl**, se solicita la interacción del usuario mediante *STDIN*, por ejemplo en este caso, se recoge mediante el uso de *STDIN* el valor de dos variables que el usuario escribe:

.. code-block:: perl

    #!/usr/bin/perl
    print "Introduce tu nombre:\n";
    my $nombre = <STDIN>;
    print "Introduce tu apellido:\n";
    my $apellido = <STDIN>;


Es posible redireccionar la entrada estándar de un comando para que su procedencia sea de un archivo y no desde el teclado. En la ausencia de argumentos de nombres de archivos, muchos comandos y utilitarios de *GNU/Linux* leen su entrada desde el **STDIN**; que en la mayoría de los casos es el teclado asignado a la terminal.

La forma para redireccionar el **STDIN** es la siguiente:

.. code-block:: bash

    $ wc -l < usuarios_nuevos.txt 
    2
    $ 

En el ejemplo, el comando :bash:`wc -l` cuenta las líneas del **STDIN** que en este caso es el archivo :bash:`usuarios_nuevos.txt`.

Redireccionar la entrada estándar desde un archivo permite escribir todos los argumentos de entrada previamente, revisar y corregir errores, así como poder reusar el archivo repetidamente.

Salida estándar (stdout)
++++++++++++++++++++++++

A través de la salida estándar, **stdout** se reciben los datos que vuelca el comando o programa durante su ejecución, por ejemplo, el resultado de los comandos :bash:`ls`, :bash:`cat` o cualquier otro comando de terminal.

En cambio, hay otros comandos o programas que no muestran salida, a no ser que se le especifique, como por ejemplo mover o copiar ficheros.

En el ejemplo del *script* de **perl**, si bien cada comando :perl:`print` opera sobre la salida estándar, se puede agregar un comando que muestre por la **stdout** la información ingresada por el usuario:

.. raw:: pdf

    Spacer 0 5

.. code-block:: perl

    #!/usr/bin/perl
    print "Introduce tu nombre:\n";
    my $nombre = <STDIN>;
    print "Introduce tu apellido:\n";
    my $apellido = <STDIN>;
    chop $nombre;
    print "Hola $nombre $apellido!";

.. raw:: pdf

    Spacer 0 5

Al ejecutar el comando :bash:`ls` en un directorio, la salida de datos (stdout) se muestra en la terminal:

.. raw:: pdf

    Spacer 0 5

.. code-block:: bash

    $ ls
    binarios   Documentos  Imágenes  Plantillas  Respaldo  ttyrecord  Vídeos
    Descargas  Escritorio  Música    Público     test.pl   usuarios_nuevos.txt
    $

.. raw:: pdf

    Spacer 0 5

Para manipular la salida estándar, se utiliza el símbolo de redirección :bash:`>`. Siguiendo el ejemplo, para redirigir la **stdout** a un archivo de texto, la expresión del comando es:

.. raw:: pdf

    Spacer 0 5

.. code-block:: bash

    $ ls > listado.txt

.. raw:: pdf

    Spacer 0 5

Al redirigir la salida estándar, no aparece el resultado del comando en pantalla, sino que se ha almacenado en el archivo indicado:

.. code-block:: bash

    $ cat listado.txt
    binarios
    Descargas
    Documentos
    Escritorio
    Imágenes
    listado.txt
    Música
    Plantillas
    Público
    Respaldo
    test.pl
    ttyrecord
    usuarios_nuevos.txt
    Vídeos
    $


Error estándar (stderr)
+++++++++++++++++++++++

A través del canal **stderr** los programas o comandos envían el informe de error de la ejecución de un comando. En los *shell scripts* o comandos se puede combinar el uso de **stdout** y **stderr** para separar la salida estándar de los errores, almacenarlos en registros independientes o manipularlos por separado. Por defecto la salida de errores se vuelca en la terminal.

Ejemplo con parámetros incorrectos:

.. code-block:: bash

    $ ls --este_parametro_no_existe
    ls: opción no reconocida «--este_parametro_no_existe»
    Pruebe 'ls --help' para más información.

    $ 

Para guardar la salida del canal **stderr** se vuelcan en un archivo redireccionándo la salida. Para realizar la redirección se debe utilizar el número :bash:`2`, asociado a la **stderr**, seguido del símbolo de redirección :bash:`>`.

Si quisieramos almacenar los errores de la ejecución del comando en caso de suceder, podemos volcarlos usando en lugar del símbolo :bash:`>` el número :bash:`2` seguido de :bash:`>`:

.. code-block:: bash

    $ ls --este_parametro_no_existe 2> error.log

Los errores ya no aparecen en pantalla, son guardados en el archivo :bash:`error.log`:

.. code-block:: bash

    $ cat error.log 
    ls: opción no reconocida «--este_parametro_no_existe»
    Pruebe 'ls --help' para más información.
    
    $ 

Combinación de salidas y opciones especiales
++++++++++++++++++++++++++++++++++++++++++++

Se puede redirigir tanto la **stderr** como la **stdout** a archivos separados para tener un registro de los resultados del comando:

.. code-block:: bash

    $ cd /home
    $ ls -R > $HOME/salidals.txt 2> $HOME/erroresls.txt
    $ cd


Entonces el contenido de la salida estándar **stdout** se guarda en el archivo :bash:`salidals.txt`:

.. code-block:: bash

    $ cat salidals.txt 
    .:
    administrador
    alovelace
    aswartz
    aturing
    lmartinez
    lost+found
    ltorvalds
    
    ./administrador:
    binarios
    Descargas
    Documentos
    erroresls.txt
    error.log
    Escritorio
    Imágenes
    listado.txt
    Música
    Plantillas
    Público
    Respaldo
    salidals.txt
    test.pl
    ttyrecord
    usuarios_nuevos.txt
    Vídeos
    
    ./administrador/Descargas:
    google-chrome-stable_current_amd64.deb
    vivaldi-stable_1.4.589.11-1_amd64.deb

    ./administrador/Documentos:
    trabajo_práctico_2

    ./administrador/Documentos/trabajo_práctico_2:
    Administración
    DeptoTI
    Personal
    Ventas
    
    ./administrador/Documentos/trabajo_práctico_2/Administración:
    
    ./administrador/Documentos/trabajo_práctico_2/DeptoTI:
    
    ./administrador/Documentos/trabajo_práctico_2/Personal:
    
    ./administrador/Documentos/trabajo_práctico_2/Ventas:
    
    ./administrador/Escritorio:
    
    ./administrador/Imágenes:
    
    ./administrador/Música:
    
    ./administrador/Plantillas:
    
    ./administrador/Público:
    
    ./administrador/Respaldo:
    201607
    
    ./administrador/Respaldo/201607:
    Documentos
    
    ./administrador/Respaldo/201607/Documentos:
    classified.pdf
    confidential.pdf
    default.pdf
    default-testpage.pdf
    form_english.pdf
    form_russian.pdf
    secret.pdf
    standard.pdf
    topsecret_copia_respaldo.pdf
    topsecret.pdf
    unclassified.pdf
    
    ./administrador/Vídeos:
    
    ./alovelace:
    
    ./aswartz:
    
    ./aturing:
    
    ./lmartinez:
    
    ./ltorvalds:

    $ 


Y el contenido del canal error estándar **stderr** se guarda en el archivo :bash:`erroresls.txt`:

.. code-block:: bash

    $ cat erroresls.txt 
    ls: no se puede abrir el directorio './lost+found': Permiso denegado

    $ 


Una opción interesante es la posibilidad de redirigir tanto la **stderr** como la **stdout** al mismo sitio, en lugar de especificar dos veces el mismo destino se utiliza el modo :bash:`2>&1`.

.. code-block:: bash

    $ cd /home
    $ ls -R > $HOME/salidalsfull.txt 2>&1
    $ cd

En este caso el archivo incluye la información de ambas salidas.

.. code-block:: bash

    $ cat salidalsfull.txt 
    .:
    administrador
    alovelace
    aswartz
    aturing
    lmartinez
    lost+found
    ltorvalds
    
    ./administrador:
    binarios
    Descargas
    Documentos
    erroresls.txt
    error.log
    Escritorio
    Imágenes
    listado.txt
    Música
    Plantillas
    Público
    Respaldo
    salidalsfull.txt
    salidals.txt
    test.pl
    ttyrecord
    usuarios_nuevos.txt
    Vídeos
    
    ./administrador/Descargas:
    google-chrome-stable_current_amd64.deb
    vivaldi-stable_1.4.589.11-1_amd64.deb
    
    ./administrador/Documentos:
    trabajo_práctico_2
    
    ./administrador/Documentos/trabajo_práctico_2:
    Administración
    DeptoTI
    Personal
    Ventas
    
    ./administrador/Documentos/trabajo_práctico_2/Administración:
    
    ./administrador/Documentos/trabajo_práctico_2/DeptoTI:
    
    ./administrador/Documentos/trabajo_práctico_2/Personal:
    
    ./administrador/Documentos/trabajo_práctico_2/Ventas:
    
    ./administrador/Escritorio:
    
    ./administrador/Imágenes:
    
    ./administrador/Música:
    
    ./administrador/Plantillas:
    
    ./administrador/Público:
    
    ./administrador/Respaldo:
    201607
    
    ./administrador/Respaldo/201607:
    Documentos
    
    ./administrador/Respaldo/201607/Documentos:
    classified.pdf
    confidential.pdf
    default.pdf
    default-testpage.pdf
    form_english.pdf
    form_russian.pdf
    secret.pdf
    standard.pdf
    topsecret_copia_respaldo.pdf
    topsecret.pdf
    unclassified.pdf
    
    ./administrador/Vídeos:
    
    ./alovelace:
    
    ./aswartz:
    
    ./aturing:
    
    ./lmartinez:
    ls: no se puede abrir el directorio './lost+found': Permiso denegado
    
    ./ltorvalds:
    
    $ 


Otra opción es redirigir tanto **stdout** como **stderr** directamente a la *basura* utlizando el archivo especial :bash:`/dev/null`. De esta manera no se genera actividad en la pantalla logrando una ejecución silenciosa, muy útil en la ejecución de comandos dentro de un *shell script*.

.. code-block:: bash

    $ ls -l > /dev/null 2>&1


Es normal redireccionar la salida a un archivo existente, cualquier contenido de éste archivo será totalmente eliminado si utilizamos el símbolo :bash:`>`. Cuando es necesario preserver el contenido del archivo, registros de *logs* por ejemplo, se debe utilizar el símbolo :bash:`>>` que le indica al comando que debe agregar la información al continuación del contenido actual del archivo.

Tuberías (pipes)
++++++++++++++++

Las tuberías son como la redirección, pero, trabajan un poco diferente a ellas. Las tuberías permiten que los flujos de *I/O* (entrada/salida) de una serie de procesos sean conectados, encadenando de esta forma los comandos de una tubería a otra.

El carácter de tubería :bash:`|` le indica al *shell* que conecte la salida estándar **stdout** del comando a la izquierda, a la entrada estándar **stdin** del comando en la derecha.

Por ejemplo, para buscar en el fichero :bash:`/etc/passwd` todas las líneas que contengan la palabra *Alumno* se debe utilizar el comando :bash:`cat` para mostrar el archivo :bash:`/etc/passwd` y pasarle el resultado al comando :bash:`grep` para que este busque las coincidencias.

.. code-block:: bash

    $ cat /etc/passwd | grep Alumno
    ltorvalds:x:1003:1003:Linus Torvalds,,,,Alumno:/home/ltorvalds:/bin/bash
    alovelace:x:1002:1002:Ada Lovelace,,,,Alumno:/home/alovelace:/bin/bash
    aturing:x:1004:1004:Alan Turing,,,,Alumno:/home/aturing:/bin/bash
    aswartz:x:1005:1006:Aaron Swartz,,,,Alumno:/home/aswartz:/bin/bash
    
    $ 

Los comandos se pueden encadenar unos a otros realizando operaciones sobre la **stdout** del comando anterior. Por ejempo, si lo que se necesita es saber es la cantidad de usuarios que son *alumnos* se ejecuta lo siguiente:

.. code-block:: bash

    $ cat /etc/passwd | grep Alumno | wc -l
    4
    
    $


Creación de shell scripts
-------------------------

Como todos los antecedentes de *UNiX*, *GNU/Linux* tiene su propia manera de agrupar comandos en un archivo ejecutable, conocido en el mundo *UNiX* como *Scripts del Shell*. Éstos *scripts* juegan un rol esencial, por la característica de multitareas del *shell* de *GNU/Linux*, que puede llevar a cabo muchas cosas simultáneas, y por ende complejas, desde *shell scripts* que sirven en el rol de guión para orquestar, monitorear y coordinar toda esta complejidad.

En esencia y funcionalidad, un *shell script* desempeña el mismo papel que cualquier otro archivo ejecutable. Este puede leer desde la entrada estándar **STDIN**, escribir a la salida estándar **STDOUT** y al error estándar **STDERR**. También es posible abrir y cerrar archivos.

Se puede abrir una base de datos, editarla, y cerrarla. En definitiva, toda tarea que se pueda llevar a cabo desde la línea de
comandos, se puede efectúar desde un *shell script*. Los *scripts* son muy usados en las partes más criticas de los sistemas *GNU/Linux*. La inicialización del sistema, inmediatamente después del gestor de arranque, es manejada mayormente por *shell scripts*. El inicio del Sistema **X Window** es normalmente manejado por un *shell script*.

Los *Scripts de Shell* son, simples archivos de texto, normalmente creados por editores de texto como :bash:`vi`, :bash:`emacs`, :bash:`nano` o por cualquier flujo de texto que produzca un archivo. Así, que un *shell script*, puede generar, y luego ejecutar, otro *shell script* en tiempo de ejecución, y luego eliminarlos después que ya no sean necesitados.

Un *shell script* es un archivo de texto que contiene comandos de *shell*. El intérprete **Bash** lee y ejecuta los comando del archivo y sale, este modo operativo se denomina *shell no-interactivo*. 

Para hacer ejecutable el archivo del *shell script* se debe asignar el permiso de ejecución al archivo utilizando el comando :bash:`chmod`.

.. code-block:: bashrc

    $ chmod +x mi_script.sh


Crear archivos scripts desde cero, involucra cuatro pasos:

1. Identificar qué es lo que desea efectuar con el *shell script*.
2. Determinar con cuál o cuales comandos efectúa la acción deseada.
3. Usar un editor para crear la secuencia de comandos en un archivo de texto.
4. Usando el comando :bash:`chmod` asignarle los *bits* de permisos de ejecución al archivo.

.. raw:: pdf

    Spacer 0 10

Algunas consideraciones de buenas prácticas al momento de desarrollar *shell scripts*:

* Un *shell script* tiene que ejecutarse sin errores.
* Tiene que realizar la tarea para la que fue creado.
* La lógica del programa debe ser clara y bien definida.
* Un *shell script* no realiza tareas innecesarias.
* Un *shell script* tiene que ser reutilizable.
* Responder algunas preguntas guía:

  * ¿Se necesita información del usuario o del entorno de éste?
  * ¿Cómo se va a guardar esa información?
  * ¿Hay que crear archivos? ¿Dónde y con qué permisos?
  * ¿Qué comandos se usarán? Si se va a usar el *shell script* en sistemas diferentes, ¿están disponibles estos comandos?
  * ¿Hay que notificar al usuario? ¿cuándo y por qué?

.. raw:: pdf

    Spacer 0 10

La estructura de un *shell script* es muy flexible. Sin embargo, se debe asegurar una lógica correcta, control de flujo y eficiencia para que los usuarios puedan utilizarlo más fácil.


Ejecución de un shell script
++++++++++++++++++++++++++++

Cuando **Bash** ejecuta el *shell script* asigna al parámetro especial :bash:`0` el nombre del archivo y al resto de los parámetros posicionales les asigna en los argumentos dados, si es que existen.
Un *shell script* puede ejecutarse llamando al intérprete y pasando el *shell script* y sus argumentos como argumentos de **Bash** o usando el nombre del *shell script* si tiene el *bit* de ejecución, así las opciones son:

.. code-block::  bash

    $ bash mi_script.sh argumento1 argumento2 argumentoN

    $ mi_script.sh argumento1 argumento2 argumentoN


Si la primer línea del *shell script* comienza con los caracteres :bash:`#!` el resto de la línea indica el intérprete de comandos a utilizar, ya sea **Bash**, **awk**, **Perl** u otro intérprete disponible en el sistema, y continuar el *script* en el lenguaje correspondiente.

Los *shell scripts* de **Bash** comienzan con :bash:`#!/bin/bash` asegurando que éste sea el intérprete sin importar si se ejecutan bajo el entorno de otro *shell*.


Aspectos generales
++++++++++++++++++

Si la línea no es un comentario, es decir (dejando de lado los espacios en blanco y tabulador) la cadena no comienza por :bash:`#`, el *shell script* lee y lo divide en palabras y operadores, que se convierten en comandos, operadores y otras construcciones. A partir de este momento, se realizan las expansiones y sustituciones, las redirecciones, y finalmente, la ejecución de los comandos.

En **Bash** se pueden tener funciones, que son una agrupación de comandos que se pueden invocar posteriormente. Y cuando se invoca el nombre de la *función de shell*, como un nombre de comando simple, la lista de comandos relacionados con el nombre de la función se ejecutará teniendo en cuenta que las funciones se ejecutan en el contexto del *shell script* en curso sin crear ningún proceso nuevo para ello.

Un *parámetro* es una entidad que almacena valores, que puede ser un nombre, un número o un valor especial. Una *variable* es un parámetro que almacena un valor.

Por último, existen un conjunto de expansiones que realiza el *shell* que se lleva a cabo en cada línea y que pueden ser enumeradas como: de tilde/comillas, parámetros y variables, substitución de comandos, de aritmética, de separación de palabras y de nombres de archivos.

Ejemplos de script
++++++++++++++++++

Para construir el archivo **PDF** con el texto de los apuntes de la materia se utiliza el *shell script* :bash:`crea_pdf.sh` logrando que no sea necesario recordar los parámetros del comando :bash:`rst2pdf` necesarios.

.. code-block:: bash    

    #!/bin/bash

    rst2pdf ${1}.rst ../ADM-GNU-LINUX-I/${1}.pdf -s estilos_propios/tusl2.style --re
    peat-table-rows -e preprocess

Se observa el uso del parámetro :bash:`$1` donde el :bash:`1` está entre llaves para separarlo de la cadena de caracteres contigua. Este parámetro toma el valor del primer argumento que acompaña al comando cuando se lo ejecuta:

.. code-block:: bash
    
    $ crea_pdf.sh Unidad_V

La carátula del apunte, que se realiza a partir de un documento *SVG* editado con el programa *inkskape*, también es un archivo **PDF** que se debe unir al documento del apunte. Para ello se utiliza el comando :bash:`gs` con una serie de parámetros que, al igual que los del comando :bash:`rst2pdf` sería engorroso recordar y muy fácil cometer errores al escribirlos cada vez que se genera el documento. La solución es utilizar un *shell script* similar:

.. code-block:: bash

    $ cat armapdf.sh 
    #!/bin/bash

    gs -q -sPAPERSIZE=a4 -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=Unidad_${1
    }_Final.pdf Unidad_${1}_caratula.pdf Unidad_${1}.pdf 

Y la forma de ejecutarlo es: 

.. code-block:: bash
    
    $ armapdf.sh V



En el ejemplo siguiente se muestra un caso sencillo de obtención de información del sistema.

Para ver el contenido se utiliza el comando :bash:`cat`, el uso del parámetro :bash:`-n` permite visualizar el número de línea de cada comando.

.. code-block:: bash

    $ cat -n info.sh
    1 #!/bin/bash
    2 clear; echo "Información dada por el shell script info.sh."
    3 echo "Hola, $USER"
    4 echo
    5 echo "La fecha es `date`, y esta semana `date +"%V"`."
    6 echo
    7 echo "Usuarios conectados:"
    8 w | cut -d " " -f 1 - | grep -v USER | sort -u
    9 echo
    10 echo "El sistema es `uname -s` y el procesador es `uname -m`."
    11 echo
    12 echo "El sistema está encendido desde hace:"
    13 uptime
    14 echo
    15 echo "¡Esto es todo amigos!"

La línea *1* corresponde a la definición del intérprete.

La línea *2* es una combinación de dos comandos separados por el caracter :bash:`;`. El comando :bash:`clear` limpia la pantalla del terminal y luego el comando :bash:`echo` muestra en la *stdout* el texto indicado entre comillas.

En la línea *3* el comando :bash:`echo` muestra en pantalla el texto entre comillas que será *expandido* con el valor de la variable **$USER**.

Las líneas *4*, *6*, *9*, *11* y *14* ejecutan el comando :bash:`echo` sin argumentos, esto hace que en la terminal se muestre una línea en blanco utilizada para separar el resultado del resto de los comandos del *shell script*.

En la línea *5* se muestra en pantalla un texto que, en este caso, se *expande* con el resultado de las llamadas al sistema que ejecutan el comando :bash:`date`. En la línea *10* sucede lo mismo con el comando :bash:`uname`.

Las líneas *7*, *12* y *15* hacen un uso simple del comando :bash:`echo` mostrando el texto entre comillas.

La línea *8* es la más interesante del *shell script* ya que utiliza *tuberías* para redireccionar la salida de un comando hacia la entrada de otro. El comando :bash:`w` lista los usuarios conectados al sistemas, esa información se pasa al comando :bash:`cut` que separa en campos delimitados por un espacio y selecciona el primer campo. El resultado se pasa al comando :bash:`grep` que selecciona todas las líneas y descarta aquellas que tengan la cadena :bash:`USER` para luego pasar la información al comando :bash:`sort` que muestra el listado de usuarios eliminando las coincidencias para que aparezcan una sola vez.

En la línea *13* se ejecuta el comando :bash:`uptime` directamente. 

La ejecución del *shell script* presenta el siguiente resultado:

.. code-block:: bash

    Información dada por el shell script info.sh. 
    Hola, leonardo

    La fecha es mié sep 21 10:32:42 ART 2016, y esta semana 38.

    Usuarios conectados:

    leonardo

    El sistema es Linux y el procesador es x86_64.

    El sistema está encendido desde hace:
     10:32:42 up  2:02,  3 users,  load average: 0,23, 0,27, 0,20
    
    ¡Esto es todo amigos!





 


.. Cierre de apunte con bibliografía

.. raw:: pdf

    PageBreak

Bibliografía
============

* Páginas de Manual (Man Pages) del sistema operativo.

.. raw:: pdf

    Spacer 0 10

La bibliografía que se indica a continuación corresponde a material que provee una visión interesante sobre los temas propuestos en esta unidad. Es documentación más completa e incluso más extensiva en el desarrollo de algunos temas.

* `The Debian Administrator's Handbook <https://debian-handbook.info/>`_, Raphaël Hertzog and Roland Mas

* `Administración de sistemas GNU/Linux <http://ocw.uoc.edu/informatica-tecnologia-y-multimedia/administracion-de-sistemas-gnu-linux-1>`_, Máster universitario en Software Libre, Universitat Oberta de Catalunya

* `Básicamente GNU/Linux <http://www.codigolibre.org>`_, Antonio Perpiñan, Fundación Código Libre Dominicano

* `Manual de Referencia de Bash (Bash Reference Manual) <https://www.gnu.org/software/bash/manual/bashref.html>`_
 
* `How To Read and Set Environmental and Shell Variables on a Linux VPS <https://www.digitalocean.com/community/tutorials/how-to-read-and-set-environmental-and-shell-variables-on-a-linux-vps>`_, Justin Ellingwood

* `Bash Guide for Beginners <http://www.tldp.org/LDP/Bash-Beginners-Guide/html/>`_, Machtelt Garrels
