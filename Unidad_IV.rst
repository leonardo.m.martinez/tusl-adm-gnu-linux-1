.. header:: 
    Administración de GNU/Linux I - Unidad 4 - Gestión de Software - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. role:: bash(code)
    :language: bash

.. Agregamos la Carátula definida en el estilo

.. Activamos el numerado de las secciones
.. sectnum::

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
    PageBreak


Copyright © 2016.

:Autor:       Leonardo Martinez  

.. 
    :Colaborador: 
    :Colaborador: 
    :Colaborador: 

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución Compartir Igual (CC-BY-SA) 4.0 Internacional. `<http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/licencia.png 

.. raw:: pdf

    PageBreak 
..    SetPageCounter 1 arabic


Gestión de Software
===================

El software de las distribuciones *GNU/Linux* se distribuye en un formato llamado paquete. Un paquete es una colección de archivos combinados para formar un software. Este archivo además contiene información especial sobre el contenido, así como dependencias, que son otros paquetes necesarios para su funcionamiento apropiado.

Una de las funciones del administrador del sistemas de linux es gestionar estos paquetes ya sea instalando, actualizándolo, removiéndolo o verificándolo.

En las distribuciones de *GNU/Linux*, los **paquetes** son el elemento básico para tratar las tareas de instalación de nuevo software, actualización del existente o eliminación del no utilizado. El uso de paquetes facilita añadir o quitar software, al considerarlo una unidad y no tener que trabajar con los archivos individuales.

    Básicamente, un **paquete** es un conjunto de archivos que integran una aplicación o una unión de varias aplicaciones relacionadas, formando un único archivo, con un formato propio y comprimido, que es el que se distribuye, ya sea vía CD o mediante acceso a servicios de red del tipo *ftp* o *http*.

Algunas aplicaciones o componentes son distribuidos en más de un paquete, separando los esenciales de aquellos opcionales o relacionados con desarrollo, *plugins* de usuario o componentes modulares. Además del paquete principal, podemos encontrar paquetes que ofrezcan los archivos comunes asociados, los archivos asociados a desarrollo u otras denominaciones para archivos extra de la aplicación o componente del sistema.


Gestión de paquetes en Debian, Ubuntu y derivadas
-------------------------------------------------

Un administrador de sistemas de distribuciones *GNU/Linux* *Debian*, *Ubuntu* o derivadas, generalmente manejará paquetes **.deb** que contienen unidades funcionales consistentes (aplicaciones, documentación, etc.) que facilitan la instalación y mantenimiento de software. Por lo tanto, es buena idea saber qué son y cómo utilizarlos.

    "Lo que hace a Debian tan popular entre administradores es lo sencillo que resulta instalar software y lo fácil que se puede actualizar el sistema completo. Esta ventaja única es en gran parte debido al programa APT." The Debian Administrator’s Handbook

Un paquete *Debian* no es sólo un compendio de archivos a instalar. Es parte de un todo más grande y describe su relación con otros paquetes *Debian* informando dependencias, conflictos y sugerencias. También provee *scripts* que permiten la ejecución de comandos en diferentes estapas del ciclo de vida del paquete: durante la instalación, la eliminación y la actualización. Estos datos utilizados por las herramientas de gestión de paquetes no son parte del software empaquetado pero son, dentro del paquete, lo que se denomina *metainformación*.


Los paquetes **.deb**
+++++++++++++++++++++

El formato del paquete *Debian* fue diseñado para que su contenido pueda ser extraído en cualquier sistema *Unix* que tenga los programas clásicos :bash:`ar`, :bash:`tar` y :bash:`gzip`. Esta propiedad aparentemente trivial es importante para la portabilidad y recuperación en caso de desastres.

En el caso de elimiar por error el programa :bash:`dpkg` ya no se puede instalar paquetes *Debian* en el sistema. Como el programa es en sí mismo un paquete, Se podría pensar que el sistema está comprometido y no se podrá instalar más aplicaciones. Sin embargo, el formato de un paquete es claro y cono sólo descargar el archivo **.deb** que corresponde al paquete :bash:`dpkg` e instalarlo manualmente.

Si por cualquier motivo o problema uno o más de los programas :bash:`ar`, :bash:`tar` o :bash:`gzip` no estuvieran presentes, se pueden copiar de otro sistema, ya funcionan de forma completamente autónoma.

Las dependencias están definidas en la cabecera del paquete. Esta es una lista de condiciones a cumplir para que un paquete funcione correctamente en el sistema. Algunas de las herramientas de gestión de paquetes utilizan esta información para instalar los paquetes necesarios, las versiones apropiadas, de las que depende el paquete a instalar.


Debian tiene herramientas interactivas como :bash:`tasksel`, que permiten escoger subconjuntos de paquetes agrupados por tipo de tareas: paquetes para entorno X, para desarrollo, para documentación, etc.

Otra herramienta es :bash:`dselect` que facilita navegar por la lista de paquetes disponibles y seleccionar aquellos que se quieren instalar o desinstalar. ambos comandos son solo un *front-end* del gestor de software **APT**.

En el nivel de línea de comandos existe de :bash:`dpkg` como el comando de más bajo nivel, para gestionar directamente los paquetes *DEB* de software.

Las herramientas **APT** proveen un nivel más complejo de administración de paquetes mediante el uso de de listas de paquetes asociadas a repositorios o fuentes de software accesibles localmente (CDs ó DVDs) o remotos en sitios *FTP* o web (*HTTP*).

Los paquetes de software están disponibles para las diferentes versiones de la distribución *Debian*. Existen paquetes para las versiones *stable*, *testing* y *unstable*. El uso de unos u otros determina el tipo de distribución que se decide utilizar en la instalación. Pueden tenerse fuentes de paquetes mezcladas, pero no es muy recomendable, ya que se podrían dar conflictos
entre las versiones de las diferentes distribuciones.

El programa :bash:`dpkg`
++++++++++++++++++++++++

El programa :bash:`dpkg` es la base para manejar los paquetes **.deb** de los sistemas *GNU/Linux Debian*. El comando permite instalar o analizar sus contenidos, pero sólo tiene una visión parcial del universo *Debian*: sabe lo que está instalado en el sistema y lo que sea que se le provee en la línea de órdenes, pero no sabe nada más de otros paquetes disponibles. Como tal, fallará si no se satisface una dependencia. Por el contrario, herramientas como :bash:`apt` crearán una lista de dependencias a instalar tan automáticamente como les sea posible.


Instalación de paquetes
~~~~~~~~~~~~~~~~~~~~~~~

Para el uso de :bash:`dpkg` es necesario disponer del paquete **.deb**, que se puede obtener descargando de internet o copiando desde otra fuente.

.. code-block:: bash

    $ sudo dpkg -i <nombre_paquete>.deb

    $ sudo dpkg -i google-chrome-stable_current_amd64.deb

El parámetro :bash:`--install` es equivalente.


Quitar paquete del sistema
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

    $ sudo dpkg -r <nombre_paquete>

    $ sudo dpkg -r google-chrome-stable_current_amd64

El parámetro :bash:`--remove` es equivalente.


Purga de un paquete
~~~~~~~~~~~~~~~~~~~

Al purgar un paquete se elimina también los archivos de configuración del mismo. SI un paquete fue eliminado, quitará del sistema los archivos de configuración que hayan quedado.

.. code-block:: bash

    $ sudo dpkg -P <nombre_paquete>

    $ sudo dpkg -P google-chrome-stable_current_amd64

El parámetro :bash:`--purge` es equivalente.


Consultas sobre los paquetes del sistema
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
El comando :bash:`dpkg` tiene algunos parámetros para realizar consultas sobre los paquetes instalados.

Con el parámetro :bash:`-l` muestra un listado de paquetes que coincidan con el patrón de búsqueda indicado. En caso de no utiliarse sin patrón de búsqueda, listará la totalidad de los paquetes disponibles.


.. code-block:: bash
    
    $ sudo dpkg -l mc*
    Deseado=desconocido(U)/Instalar/eliminaR/Purgar/retener(H)
    | Estado=No/Inst/ficheros-Conf/desempaqUetado/medio-conF/medio-inst(H)/espera-di
    sparo(W)/pendienTe-disparo
    |/ Err?=(ninguno)/requiere-Reinst (Estado,Err: mayúsc.=malo)
    ||/ Nombre         Versión      Arquitectura Descripción
    +++-==============-============-============-=================================
    ii  mc             3:4.8.15-2   amd64        Midnight Commander - a powerful f
    ii  mc-data        3:4.8.15-2   all          Midnight Commander - a powerful f
    un  mcedit         <ninguna>    <ninguna>    (no hay ninguna descripción dispo
    un  mcollective    <ninguna>    <ninguna>    (no hay ninguna descripción dispo
    administrador@xubuntu1604:~/Descargas$ 
    $


Con el parámetro :bash:`-s` muestra el estado de un paquete.

.. code-block:: bash

    $ sudo dpkg -s mc
    Package: mc
    Status: install ok installed
    Priority: optional
    Section: utils
    Installed-Size: 1430
    Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
    Architecture: amd64
    Version: 3:4.8.15-2
    Provides: mcedit
    Depends: e2fslibs (>= 1.37), libc6 (>= 2.15), libglib2.0-0 (>= 2.35.9), libgpm2
    (>= 1.20.4), libslang2 (>= 2.2.4), libssh2-1 (>= 1.2.5), mc-data (= 3:4.8.15-2)
    Recommends: mime-support, perl, unzip
    Suggests: arj, bzip2, catdvi | texlive-binaries, dbview, djvulibre-bin, file, ge
    nisoimage, gv, imagemagick, links | w3m | lynx, odt2txt, poppler-utils, python, 
    python-boto, python-tz, xpdf | pdf-viewer, zip
    Conffiles:
     /etc/mc/edit.indent.rc 008c6d0205315a87a977c7cd74a1526e
     /etc/mc/filehighlight.ini 7a3c0b379b4618d5463a7d408c7d1a3e
     /etc/mc/mc.default.keymap 2fefb0f51ff14cbc7c83a2f82429b95c
     /etc/mc/mc.emacs.keymap f9932588d0dfa0b44558991f97ffbcef
     /etc/mc/mc.ext d3a9511c2dc35421f4aca1fe5e1d59c8
     /etc/mc/mc.keymap 2fefb0f51ff14cbc7c83a2f82429b95c
     /etc/mc/mc.menu a316f41bd61d4dec82a7772571321160
     /etc/mc/mc.menu.sr eb59ac75f43f9edd8aacee674f79b7fe
     /etc/mc/mcedit.menu 270e654bfefa735c848ca73cdff84db6
     /etc/mc/sfs.ini 316dc92f3fdec60a7aaf0866edc361db
    Description: Midnight Commander - a powerful file manager
     GNU Midnight Commander is a text-mode full-screen file manager. It
     uses a two panel interface and a subshell for command execution. It
     includes an internal editor with syntax highlighting and an internal
     viewer with support for binary files. Also included is Virtual
     Filesystem (VFS), that allows files on remote systems (e.g. FTP, SSH
     servers) and files inside archives to be manipulated like real files.
    Original-Maintainer: Debian MC Packaging Group <pkg-mc-devel@lists.alioth.debian
    .org>
    Homepage: http://www.midnight-commander.org
    
    $ 

   
Con el parámetro :bash:`-L` muestra el listado de archivos instalados en el sistema que pertenecen el paquete.

.. code-block:: bash

    $ sudo dpkg -L acl 
    /.
    /bin
    /bin/chacl
    /bin/setfacl
    /bin/getfacl
    /usr
    /usr/bin
    /usr/share
    /usr/share/doc
    /usr/share/doc/acl
    /usr/share/doc/acl/copyright
    /usr/share/doc/acl/README
    /usr/share/doc/acl/PORTING
    /usr/share/man
    /usr/share/man/man1
    /usr/share/man/man1/chacl.1.gz
    /usr/share/man/man1/setfacl.1.gz
    /usr/share/man/man1/getfacl.1.gz
    /usr/share/man/man5
    /usr/share/man/man5/acl.5.gz
    /usr/share/doc/acl/changelog.Debian.gz
    
    $ 


Con el parámetro :bash:`-S` se puede buscar los archivos que coincidan con el patrón de búsqueda e identificar a que paquete pertenecen.

.. code-block:: bash

    $ sudo dpkg -S libreoffice-writer
    gnome-accessibility-themes: /usr/share/icons/HighContrast/48x48/apps/libreoffice
    -writer.png
    xubuntu-icon-theme: /usr/share/icons/elementary-xfce/apps/32/libreoffice-writer.
    png
    xubuntu-icon-theme: /usr/share/icons/elementary-xfce/apps/64/libreoffice-writer.
    png
    libreoffice-writer: /usr/share/bug/libreoffice-writer/presubj
    libreoffice-common: /usr/share/icons/hicolor/48x48/apps/libreoffice-writer.png
    gnome-accessibility-themes: /usr/share/icons/HighContrast/22x22/apps/libreoffice
    -writer.png
    libreoffice-common: /usr/share/icons/hicolor/scalable/apps/libreoffice-writer.sv
    g
    libreoffice-writer: /usr/lib/mime/packages/libreoffice-writer
    libreoffice-writer: /usr/share/applications/libreoffice-writer.desktop
    libreoffice-common: /usr/share/icons/hicolor/256x256/apps/libreoffice-writer.png
    xubuntu-icon-theme: /usr/share/icons/elementary-xfce/apps/24/libreoffice-writer.
    png
    xubuntu-icon-theme: /usr/share/icons/elementary-xfce/apps/16/libreoffice-writer.
    png
    libreoffice-common: /usr/share/icons/locolor/16x16/apps/libreoffice-writer.png
    libreoffice-common: /usr/share/icons/hicolor/16x16/apps/libreoffice-writer.png
    xubuntu-icon-theme: /usr/share/icons/elementary-xfce/apps/22/libreoffice-writer.
    png
    libreoffice-common: /usr/share/icons/locolor/32x32/apps/libreoffice-writer.png
    libreoffice-common: /usr/share/icons/gnome/16x16/apps/libreoffice-writer.png
    libreoffice-writer: /usr/share/doc/libreoffice-writer
    libreoffice-writer: /usr/share/bug/libreoffice-writer/control
    libreoffice-writer: /usr/share/lintian/overrides/libreoffice-writer
    gnome-accessibility-themes: /usr/share/icons/HighContrast/256x256/apps/libreoffi
    ce-writer.png
    libreoffice-writer: /usr/share/appdata/libreoffice-writer.appdata.xml
    libreoffice-common: /usr/share/icons/gnome/256x256/apps/libreoffice-writer.png
    gnome-accessibility-themes: /usr/share/icons/HighContrast/32x32/apps/libreoffice
    -writer.png
    app-install-data: /usr/share/app-install/desktop/libreoffice-writer:libreoffice-
    writer.desktop
    gnome-accessibility-themes: /usr/share/icons/HighContrast/24x24/apps/libreoffice
    -writer.png
    libreoffice-common: /usr/share/icons/gnome/48x48/apps/libreoffice-writer.png
    xubuntu-icon-theme: /usr/share/icons/elementary-xfce/apps/48/libreoffice-writer.
    png
    xubuntu-icon-theme: /usr/share/icons/elementary-xfce/apps/128/libreoffice-writer
    .png
    libreoffice-common: /usr/share/icons/hicolor/128x128/apps/libreoffice-writer.png
    libreoffice-common: /usr/share/icons/gnome/128x128/apps/libreoffice-writer.png
    libreoffice-writer: /usr/share/bug/libreoffice-writer
    gnome-accessibility-themes: /usr/share/icons/HighContrast/16x16/apps/libreoffice
    -writer.png
    libreoffice-common: /usr/share/icons/gnome/scalable/apps/libreoffice-writer.svg
    app-install-data: /usr/share/app-install/icons/libreoffice-writer.svg
    libreoffice-common: /usr/share/icons/hicolor/32x32/apps/libreoffice-writer.png
    libreoffice-common: /usr/share/icons/gnome/32x32/apps/libreoffice-writer.png
    administrador@xubuntu1604:~/Descargas$ 
    
    
    $ sudo dpkg -S getfacl
    acl: /usr/share/man/man1/getfacl.1.gz
    acl: /bin/getfacl
    
    $


Si el paquete no está instalado el comando informa de la situación.

.. code-block:: bash

    $ sudo dpkg -l htop
    dpkg-query: no se ha encontrado ningún paquete que corresponda con htop.


    $ sudo dpkg -s htop
    dpkg-query: el paquete `htop' no está instalado y no hay ninguna información dis
    ponible.
    Utilice dpkg --info (= dpkg-deb --info) para examinar archivos,
    y dpkg --contents (= dpkg-deb --contents) para listar su contenido.
    
    $ 


Herramientas APT
----------------

**APT** son las siglas de *herramienta avanzada de paquetes* (*Advanced Package Tool*). Lo que hace a este programa *avanzado* es su enfoque sobre paquetes. No sólo los evalúa individual mente sino que los considera como un todo y produce la mejor combinación posible de paquetes dependiendo de lo que esté disponible y sea compatible según las dependencias.

**APT** necesita una *lista de orígenes de paquetes* o *repositorios* que publican paquetes *Debian*, esa lista se obtiene del archivo :bash:`/etc/apt/sources.list`.
**APT** importa estas listas de paquetes publicadas por cada uno de esos repositorios. Para ello, descarga los archivos :bash:`Packages.gz` o :bash:`Packages.bz2` para paquetes binarios y archivos :bash:`Sources.gz` o :bash:`Sources.bz2` para paquetes de código fuente. Al analizarlos crea un índice o base de datos de los paquetes disponibles para descargar e instalar en el sistema.

Periódicamente, en forma automática o manual, **APT** actualiza el índice descargando las diferencias y la información relacionada con los paquetes que fueron actualizados en los repositorios.

Sintaxis del archivo :bash:`/etc/apt/sources.list`
++++++++++++++++++++++++++++++++++++++++++++++++++

Cada línea del archivo :bash:`/etc/apt/sources.list` contiene una descripción de un origen, compuesta por tres partes separadas por espacios. El primer campo indica el tipo de origen, :bash:`deb` para paquetes binarios y :bash:`deb-src` paquetes de código fuente.

El segundo campo provee la *URL base* para el origen combinado con los nombres de archivos presentes en los archivos :bash:`Packages.gz`.

La sintáxis del tercer y último campo depende de si el origen corresponde a una réplica Debian o no. En el caso de una réplica Debian, debe ser el nombre la distribución elegida: *stable* , *testing* , *unstable* o sus nombre código actuales como *jessie* para la versión 8 de **Debian**. En el Caso de **Ubuntu** se utilizan los nombres, *precise* para la versión 12.04, *trusty* para la versión 14.04  y *xenial* para la versión 16.04; todas estas **LTS**. Por último se indica las secciones de paquetes a activar para el origen descripto, entre las que se encuentran: *main*, *contrib*, *non-free* para **Debian** y *main*, *restricted*, *universe*, *multiverse* para **Ubuntu**.



Ejemplo de :bash:`/etc/apt/sources.list`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El archivo de configuración de orígenes de paquetes de la máquina virtual que se utiliza de ejemplo, eliminadas las líneas de comentarios, es el siguiente:

.. code-block:: bash

    $ cat /etc/apt/sources.list
    deb http://ubuntu.unc.edu.ar/ubuntu/ xenial main restricted

    deb http://ubuntu.unc.edu.ar/ubuntu/ xenial-updates main restricted

    deb http://ubuntu.unc.edu.ar/ubuntu/ xenial universe

    deb http://ubuntu.unc.edu.ar/ubuntu/ xenial-updates universe

    deb http://ubuntu.unc.edu.ar/ubuntu/ xenial multiverse

    deb http://ubuntu.unc.edu.ar/ubuntu/ xenial-updates multiverse

    deb http://ubuntu.unc.edu.ar/ubuntu/ xenial-backports main restricted universe multiverse

    deb http://ubuntu.unc.edu.ar/ubuntu/ xenial-security main restricted

    deb http://ubuntu.unc.edu.ar/ubuntu/ xenial-security universe

    deb http://ubuntu.unc.edu.ar/ubuntu/ xenial-security multiverse

    $ 


Gestión de paquetes con las herramientas APT
++++++++++++++++++++++++++++++++++++++++++++

APT es un proyecto grande y su plan original incluía una interfaz gráfica. Está basado en una biblioteca que contiene la aplicación central y :bash:`apt-get` es la primera interfaz basada en la línea de comandos desarrollada dentro del proyecto.
El comando :bash:`apt` es un segundo *front-end* o interfaz también basado en la línea de comandos que provee **APT** que soluciona algunos problemas de diseño de :bash:`apt-get`.

Existen otras interfaces gráficas que surgieron como proyectos externos, como el caso de **synaptic**, que funciona en el entorno gráfico. La aplicación **aptitude** que se puede utilizar desde la línea de comandos o ejecutarlo sin parámetros para usar la interfaz gráfica, aunque dentro de la terminal.

A partir de la versión *16.04* de *Ubuntu* y la versión *8 (jessie)* de *Debian* la interfaz más recomendada es :bash:`apt`. Para versiones anteriores el comando utilizado para la administración de paquetes era :bash:`apt-get`.

Actualizar la base de índice de paquetes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Antes de iniciar alguna tarea de mantenimiento de software, se debe actualizar el índice, esto se lleva a cabo ejecutando el comando:

.. raw:: pdf

    Spacer 0 10

.. code-block:: bash

    $ sudo apt update
    [sudo] password for administrador: 
    Obj:1 http://ubuntu.unc.edu.ar/ubuntu xenial InRelease
    Des:2 http://ubuntu.unc.edu.ar/ubuntu xenial-updates InRelease [95,7 kB]
    Obj:3 http://ubuntu.unc.edu.ar/ubuntu xenial-backports InRelease
    Des:4 http://ubuntu.unc.edu.ar/ubuntu xenial-security InRelease [94,5 kB]
    Des:5 http://ubuntu.unc.edu.ar/ubuntu xenial-updates/main amd64 Packages [383 kB
    ]
    Des:6 http://ubuntu.unc.edu.ar/ubuntu xenial-updates/main i386 Packages [378 kB]
    Des:7 http://ubuntu.unc.edu.ar/ubuntu xenial-updates/main Translation-en [145 kB
    ]
    Des:8 http://ubuntu.unc.edu.ar/ubuntu xenial-updates/main amd64 DEP-11 Metadata 
    [301 kB]
    Des:9 http://ubuntu.unc.edu.ar/ubuntu xenial-updates/main DEP-11 64x64 Icons [18
    8 kB]
    Des:10 http://ubuntu.unc.edu.ar/ubuntu xenial-updates/universe amd64 Packages [3
    24 kB]
    Des:11 http://ubuntu.unc.edu.ar/ubuntu xenial-updates/universe i386 Packages [32
    1 kB]
    Des:12 http://ubuntu.unc.edu.ar/ubuntu xenial-updates/universe Translation-en [1
    11 kB]
    Des:13 http://ubuntu.unc.edu.ar/ubuntu xenial-updates/universe amd64 DEP-11 Meta
    data [101 kB]
    Des:14 http://ubuntu.unc.edu.ar/ubuntu xenial-updates/universe DEP-11 64x64 Icon
    s [93,1 kB]                  
    Des:15 http://ubuntu.unc.edu.ar/ubuntu xenial-security/main amd64 Packages [138 
    kB]                          
    Des:16 http://ubuntu.unc.edu.ar/ubuntu xenial-security/main i386 Packages [134 k
    B]                           
    Des:17 http://ubuntu.unc.edu.ar/ubuntu xenial-security/main Translation-en [57,0
     kB]                         
    Des:18 http://ubuntu.unc.edu.ar/ubuntu xenial-security/main amd64 DEP-11 Metadat
    a [82,1 kB]                  
    Des:19 http://ubuntu.unc.edu.ar/ubuntu xenial-security/main DEP-11 64x64 Icons [
    62,9 kB]                     
    Des:20 http://ubuntu.unc.edu.ar/ubuntu xenial-security/universe amd64 Packages [
    41,5 kB]                     
    Des:21 http://ubuntu.unc.edu.ar/ubuntu xenial-security/universe i386 Packages [4
    1,4 kB]                      
    Des:22 http://ubuntu.unc.edu.ar/ubuntu xenial-security/universe Translation-en [
    25,1 kB]                     
    Des:23 http://ubuntu.unc.edu.ar/ubuntu xenial-security/universe amd64 DEP-11 Met
    adata [2.263 B]              
    Des:24 http://ubuntu.unc.edu.ar/ubuntu xenial-security/universe DEP-11 64x64 Ico
    ns [4.105 B]                 
    Descargados 3.126 kB en 6s (469 kB/s)                                                                        
    Leyendo lista de paquetes... Hecho
    Creando árbol de dependencias       
    Leyendo la información de estado... Hecho
    Se pueden actualizar 45 paquetes. Ejecute «apt list --upgradable» para verlos.
    
    $ 

.. raw:: pdf

    Spacer 0 10

Una función interesante que incorpora :bash:`apt` es la posibilidad de ver el listado de paquetes disponibles para actualizar. Como se observa en el ejemplo hay 45 paquetes actualizables y se puede ver el listado usando los parámetros :bash:`list --upgradable`:

.. raw:: pdf

    Spacer 0 10

.. code-block:: bash

    $ sudo apt list --upgradable
    Listando... Hecho
    accountsservice/xenial-updates 0.6.40-2ubuntu11.2 amd64 [actualizable desde: 0.6
    .40-2ubuntu11.1]
    cups-browsed/xenial-updates 1.8.3-2ubuntu3.1 amd64 [actualizable desde: 1.8.3-2u
    buntu3]
    cups-filters/xenial-updates 1.8.3-2ubuntu3.1 amd64 [actualizable desde: 1.8.3-2u
    buntu3]
    cups-filters-core-drivers/xenial-updates 1.8.3-2ubuntu3.1 amd64 [actualizable de
    sde: 1.8.3-2ubuntu3]
    file-roller/xenial-updates,xenial-security 3.16.5-0ubuntu1.2 amd64 [actualizable
     desde: 3.16.5-0ubuntu1.1]
    fwupd/xenial-updates 0.7.0-0ubuntu4.3 amd64 [actualizable desde: 0.7.0-0ubuntu4.
    2]
    gnome-sudoku/xenial-updates 1:3.18.4-0ubuntu2 amd64 [actualizable desde: 1:3.18.
    2-1]
    libaccountsservice0/xenial-updates 0.6.40-2ubuntu11.2 amd64 [actualizable desde:
     0.6.40-2ubuntu11.1]
    libappstream-glib8/xenial-updates 0.5.13-1ubuntu3 amd64 [actualizable desde: 0.5
    .13-1ubuntu2]
    libcupsfilters1/xenial-updates 1.8.3-2ubuntu3.1 amd64 [actualizable desde: 1.8.3
    -2ubuntu3]
    libdfu1/xenial-updates 0.7.0-0ubuntu4.3 amd64 [actualizable desde: 0.7.0-0ubuntu
    4.2]
    libdrm-amdgpu1/xenial-updates 2.4.67-1ubuntu0.16.04.2 amd64 [actualizable desde:
     2.4.67-1ubuntu0.16.04.1]
    libdrm-intel1/xenial-updates 2.4.67-1ubuntu0.16.04.2 amd64 [actualizable desde: 
    2.4.67-1ubuntu0.16.04.1]
    libdrm-nouveau2/xenial-updates 2.4.67-1ubuntu0.16.04.2 amd64 [actualizable desde
    : 2.4.67-1ubuntu0.16.04.1]
    libdrm-radeon1/xenial-updates 2.4.67-1ubuntu0.16.04.2 amd64 [actualizable desde: 
    2.4.67-1ubuntu0.16.04.1]
    libdrm2/xenial-updates 2.4.67-1ubuntu0.16.04.2 amd64 [actualizable desde: 2.4.67
    -1ubuntu0.16.04.1]
    libegl1-mesa/xenial-updates 11.2.0-1ubuntu2.2 amd64 [actualizable desde: 11.2.0-
    1ubuntu2.1]
    libfontembed1/xenial-updates 1.8.3-2ubuntu3.1 amd64 [actualizable desde: 1.8.3-2
    ubuntu3]
    libfwupd1/xenial-updates 0.7.0-0ubuntu4.3 amd64 [actualizable desde: 0.7.0-0ubun
    tu4.2]
    libgbm1/xenial-updates 11.2.0-1ubuntu2.2 amd64 [actualizable desde: 11.2.0-1ubun
    tu2.1]
    libgl1-mesa-dri/xenial-updates 11.2.0-1ubuntu2.2 amd64 [actualizable desde: 11.2
    .0-1ubuntu2.1]
    libgl1-mesa-glx/xenial-updates 11.2.0-1ubuntu2.2 amd64 [actualizable desde: 11.2
    .0-1ubuntu2.1]
    libglapi-mesa/xenial-updates 11.2.0-1ubuntu2.2 amd64 [actualizable desde: 11.2.0
    -1ubuntu2.1]
    libnm-gtk-common/xenial-updates,xenial-updates 1.2.0-0ubuntu0.16.04.4 all [actua
    lizable desde: 1.2.0-0ubuntu0.16.04.3]
    libnm-gtk0/xenial-updates 1.2.0-0ubuntu0.16.04.4 amd64 [actualizable desde: 1.2.
    0-0ubuntu0.16.04.3]
    libnma-common/xenial-updates,xenial-updates 1.2.0-0ubuntu0.16.04.4 all [actualiz
    able desde: 1.2.0-0ubuntu0.16.04.3]
    libnma0/xenial-updates 1.2.0-0ubuntu0.16.04.4 amd64 [actualizable desde: 1.2.0-0
    ubuntu0.16.04.3]
    libp11-kit0/xenial-updates 0.23.2-5~ubuntu16.04.1 amd64 [actualizable desde: 0.2
    3.2-3]
    libpoppler-glib8/xenial-updates 0.41.0-0ubuntu1.1 amd64 [actualizable desde: 0.4
    1.0-0ubuntu1]
    libpoppler58/xenial-updates 0.41.0-0ubuntu1.1 amd64 [actualizable desde: 0.41.0-
    0ubuntu1]
    libssl1.0.0/xenial-updates 1.0.2g-1ubuntu4.2 amd64 [actualizable desde: 1.0.2g-1
    ubuntu4.1]
    libwayland-egl1-mesa/xenial-updates 11.2.0-1ubuntu2.2 amd64 [actualizable desde:
     11.2.0-1ubuntu2.1]
    libxatracker2/xenial-updates 11.2.0-1ubuntu2.2 amd64 [actualizable desde: 11.2.0
    -1ubuntu2.1]
    mesa-vdpau-drivers/xenial-updates 11.2.0-1ubuntu2.2 amd64 [actualizable desde: 1
    1.2.0-1ubuntu2.1]
    network-manager-gnome/xenial-updates 1.2.0-0ubuntu0.16.04.4 amd64 [actualizable 
    desde: 1.2.0-0ubuntu0.16.04.3]
    openssl/xenial-updates 1.0.2g-1ubuntu4.2 amd64 [actualizable desde: 1.0.2g-1ubun
    tu4.1]
    p11-kit/xenial-updates 0.23.2-5~ubuntu16.04.1 amd64 [actualizable desde: 0.23.2-
    3]
    p11-kit-modules/xenial-updates 0.23.2-5~ubuntu16.04.1 amd64 [actualizable desde:
     0.23.2-3]
    poppler-utils/xenial-updates 0.41.0-0ubuntu1.1 amd64 [actualizable desde: 0.41.0
    -0ubuntu1]
    snap-confine/xenial-updates 1.0.38-0ubuntu0.16.04.8 amd64 [actualizable desde: 1
    .0.38-0ubuntu0.16.04.4]
    snapd/xenial-updates 2.14.2~16.04 amd64 [actualizable desde: 2.12+0.16.04]
    sudo/xenial-updates 1.8.16-0ubuntu1.2 amd64 [actualizable desde: 1.8.16-0ubuntu1
    .1]
    ubuntu-core-launcher/xenial-updates 1.0.38-0ubuntu0.16.04.8 amd64 [actualizable 
    desde: 1.0.38-0ubuntu0.16.04.4]
    ubuntu-mono/xenial-updates,xenial-updates 14.04+16.04.20160804-0ubuntu1 all [act
    ualizable desde: 14.04  +16.04.20160621-0ubuntu1]
    xserver-xorg-video-intel/xenial-updates 2:2.99.917+git20160325-1ubuntu1.1 amd64 
    [actualizable desde: 2:2.99.917 +git20160325-1ubuntu1]
    
    $ 
    

.. raw:: pdf

    PageBreak

Actualizar los paquetes
~~~~~~~~~~~~~~~~~~~~~~~

Para actualizar los paquetes disponibles se utiliza el parámetro :bash:`upgrade`:

.. code-block:: bash

    $ sudo apt upgrade
    Leyendo lista de paquetes... Hecho
    Creando árbol de dependencias       
    Leyendo la información de estado... Hecho
    Calculando la actualización... Hecho
    Los paquetes indicados a continuación se instalaron de forma automática y ya no 
    son necesarios.
      linux-headers-4.4.0-28 linux-headers-4.4.0-28-generic
      linux-image-4.4.0-28-generic linux-image-extra-4.4.0-28-generic
    Utilice «sudo apt autoremove» para eliminarlos.
    Se actualizarán los siguientes paquetes:
      accountsservice cups-browsed cups-filters cups-filters-core-drivers
      file-roller fwupd gnome-sudoku libaccountsservice0 libappstream-glib8
      libcupsfilters1 libdfu1 libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2
      libdrm-radeon1 libdrm2 libegl1-mesa libfontembed1 libfwupd1 libgbm1
      libgl1-mesa-dri libgl1-mesa-glx libglapi-mesa libnm-gtk-common libnm-gtk0
      libnma-common libnma0 libp11-kit0 libpoppler-glib8 libpoppler58 libssl1.0.0
      libwayland-egl1-mesa libxatracker2 mesa-vdpau-drivers network-manager-gnome
      openssl p11-kit p11-kit-modules poppler-utils snap-confine snapd sudo
      ubuntu-core-launcher ubuntu-mono xserver-xorg-video-intel
    45 actualizados, 0 nuevos se instalarán, 0 para eliminar y 0 no actualizados.
    Se necesita descargar 20,5 MB de archivos.
    Se utilizarán 8.099 kB de espacio de disco adicional después de esta operación.
    ¿Desea continuar? [S/n]     

El comando indica cuáles son los paquetes que se van a actualizar, el tamaño de lo que se necesita descargar y el espacio adicional que será ocupado luego de la instalación de los paquetes y pregunta si se quiere continuar. Para evitar la interacción con el programa, se utiliza el parámetrp :bash:`-y` que responde afirmativamente a las preguntas durante el proceso.


Gestión de los paquetes automáticos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Una de las funcionalidades esenciales de :bash:`apt` es el seguimiento de los paquetes instalados como dependencias. Estos paquetes se denominan *automáticos*, y generalmente son bibliotecas o paquetes con archivos necesarios para el funcionamiento de una aplicación.

Cuando se eliminan paquetes, se utiliza esta información para identificar los paquetes automáticos que ya no son necesarios en el sistema.

Para quitar estos paquetes del sistema se utiliza el parámetro :bash:`autoremove`:

.. code-block:: bash

    $ sudo apt autoremove
    Leyendo lista de paquetes... Hecho
    Creando árbol de dependencias       
    Leyendo la información de estado... Hecho
    Los siguientes paquetes se ELIMINARÁN:
      linux-headers-4.4.0-28 linux-headers-4.4.0-28-generic linux-image-4.4.0-28-generic
      linux-image-extra-4.4.0-28-generic
    0 actualizados, 0 nuevos se instalarán, 4 para eliminar y 0 no actualizados.
    Se liberarán 295 MB después de esta operación.
    ¿Desea continuar? [S/n] 


Gestión de paquetes locales
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Uno de los problemas de :bash:`dpkg` es que no instala en forma automática las dependencias que requiere el paquete a instalar, por lo que primero se deben instalar las dependencias exigidas.

.. code-block:: bash

    $ sudo dpkg -i vivaldi-stable_1.4.589.11-1_amd64.deb 
    Seleccionando el paquete vivaldi-stable previamente no seleccionado.
    (Leyendo la base de datos ... 192193 ficheros o directorios instalados actualmen
    te.)
    Preparando para desempaquetar vivaldi-stable_1.4.589.11-1_amd64.deb ...
    Desempaquetando vivaldi-stable (1.4.589.11-1) ...
    dpkg: problemas de dependencias impiden la configuración de vivaldi-stable:
     vivaldi-stable depende de libappindicator1; sin embargo:
      El paquete `libappindicator1' no está instalado.
    
    dpkg: error al procesar el paquete vivaldi-stable (--install):
     problemas de dependencias - se deja sin configurar
    Procesando disparadores para gnome-menus (3.13.3-6ubuntu3.1) ...
    Procesando disparadores para desktop-file-utils (0.22-1ubuntu5) ...
    Procesando disparadores para mime-support (3.59ubuntu1) ...
    Se encontraron errores al procesar:
     vivaldi-stable
    $ 

Ente este caso, al instalar el paquete del navegador de internet *Vivaldi* (descargado del sitio web `<https://vivaldi.com/download>`_) el comando :bash:`dpkg` indica que la aplicación depende de la instalación previa del paquete :bash:`libappindicator1`.

.. code-block:: bash

    $ sudo apt install libappindicator1
    Leyendo lista de paquetes... Hecho
    Creando árbol de dependencias       
    Leyendo la información de estado... Hecho
    Tal vez quiera ejecutar «apt-get -f install» para corregirlo:
    Los siguientes paquetes tienen dependencias incumplidas:
     libappindicator1 : Depende: libindicator7 (>= 0.4.90) pero no va a instalarse
    E: Dependencias incumplidas. Intente «apt-get -f install» sin paquetes (o especi
    fique una solución).
    $ 
    

El problema puede subsistir si este paquete tiene, también, otras dependencias generando una cadena de sucesivas de instalaciones manuales de paquetes que puede hacerse realmente compleja.

A partir de la versión **1.1**, el comando :bash:`apt` permite instalar paquetes locales e indicarle que instale las dependencias utilizando el parámetro :bash:`-f`.

.. code-block:: bash

    $ sudo apt -f install ./vivaldi-stable_1.4.589.11-1_amd64.deb 
    Leyendo lista de paquetes... Hecho
    Creando árbol de dependencias       
    Leyendo la información de estado... Hecho
    Corrigiendo dependencias... Listo
    Nota, seleccionando «vivaldi-stable» en lugar de «./vivaldi-stable_1.4.589.11-1_
    amd64.deb»
    Se instalarán los siguientes paquetes adicionales:
      libappindicator1 libindicator7
    Paquetes recomendados:
      pepperflashplugin-nonfree chromium-codecs-ffmpeg-extra
    Se instalarán los siguientes paquetes NUEVOS:
      libappindicator1 libindicator7
    Se DESACTUALIZARÁN los siguientes paquetes:
      vivaldi-stable
    0 actualizados, 2 nuevos se instalarán, 1 desactualizados, 0 para eliminar y 0 n
    o actualizados.
    1 no instalados del todo o eliminados.
    Se necesita descargar 40,7 kB/46,4 MB de archivos.
    Se utilizarán 184 kB de espacio de disco adicional después de esta operación.
    ¿Desea continuar? [S/n] 
    Des:1 http://ubuntu.unc.edu.ar/ubuntu xenial/main amd64 libindicator7 amd64 12.1
    0.2+16.04.20151208-0ubuntu1 [21,9 kB]
    Des:2 http://ubuntu.unc.edu.ar/ubuntu xenial/main amd64 libappindicator1 amd64 1
    2.10.1+15.04.20141110-0ubuntu1 [18,8 kB]
    Des:3 /home/administrador/Descargas/vivaldi-stable_1.4.589.11-1_amd64.deb vivald
    i-stable amd64 1.4.589.11-1 [46,4 MB]
    Descargados 40,7 kB en 0s (44,9 kB/s)
    Seleccionando el paquete libindicator7 previamente no seleccionado.
    (Leyendo la base de datos ... 192655 ficheros o directorios instalados actualmen
    te.)
    Preparando para desempaquetar .../libindicator7_12.10.2+16.04.20151208-0ubuntu1_
    amd64.deb ...
    Desempaquetando libindicator7 (12.10.2+16.04.20151208-0ubuntu1) ...
    Seleccionando el paquete libappindicator1 previamente no seleccionado.
    Preparando para desempaquetar .../libappindicator1_12.10.1+15.04.20141110-0ubunt
    u1_amd64.deb ...
    Desempaquetando libappindicator1 (12.10.1+15.04.20141110-0ubuntu1) ...
    Preparando para desempaquetar .../vivaldi-stable_1.4.589.11-1_amd64.deb ...
    Desempaquetando vivaldi-stable (1.4.589.11-1) sobre (1.4.589.11-1) ...
    Procesando disparadores para libc-bin (2.23-0ubuntu3) ...
    Procesando disparadores para gnome-menus (3.13.3-6ubuntu3.1) ...
    Procesando disparadores para desktop-file-utils (0.22-1ubuntu5) ...
    Procesando disparadores para mime-support (3.59ubuntu1) ...
    Configurando libindicator7 (12.10.2+16.04.20151208-0ubuntu1) ...
    Configurando libappindicator1 (12.10.1+15.04.20141110-0ubuntu1) ...
    Configurando vivaldi-stable (1.4.589.11-1) ...
    update-alternatives: utilizando /usr/bin/vivaldi-stable para proveer /usr/bin/x-
    www-browser (x-www-browser) en modo     automático
    update-alternatives: utilizando /usr/bin/vivaldi-stable para proveer /usr/bin/gn
    ome-www-browser (gnome-www-browser) en modo     automático
    update-alternatives: utilizando /usr/bin/vivaldi-stable para proveer /usr/bin/vi
    valdi (vivaldi) en modo automático
    Procesando disparadores para libc-bin (2.23-0ubuntu3) ...
    $


Esto es posible en la distribución *GNU/Linux Ubuntu 16.04 LTS* que cuenta con la versión **1.2.10** desde su salida y que a setiembre de 2016 podía actualizarse a la versión **1.2.12**.

En el caso de *Debian* que, a la misma fecha sólo cuenta con la versión **1.0.9**, aún no es posible. Por lo tanto la opción es utilizar el comando :bash:`aptitude` que instalará el paquete indicado y sus dependencias para luego utilizar :bash:`dpkg` para instalar el programa descargado.

.. code-block:: bash

    $ sudo aptitude install libappindicator1


Eliminar paquetes del sistema
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Para quitar software del sistema el comando :bash:`apt` necesita utilizar el parámetro :bash:`remove`.

.. code-block:: bash

    $ sudo apt remove htop
    Leyendo lista de paquetes... Hecho
    Creando árbol de dependencias       
    Leyendo la información de estado... Hecho
    Los siguientes paquetes se ELIMINARÁN:
      htop
    0 actualizados, 0 nuevos se instalarán, 1 para eliminar y 0 no actualizados.
    Se liberarán 215 kB después de esta operación.
    ¿Desea continuar? [S/n] s
    (Leyendo la base de datos ... 192664 ficheros o directorios instalados actualmente.)
    Desinstalando htop (2.0.1-1ubuntu1) ...
    Procesando disparadores para man-db (2.7.5-1) ...
    Procesando disparadores para gnome-menus (3.13.3-6ubuntu3.1) ...
    Procesando disparadores para desktop-file-utils (0.22-1ubuntu5) ...
    Procesando disparadores para mime-support (3.59ubuntu1) ...
    
    $ 


Instalación desde orígenes PPA en Ubuntu
++++++++++++++++++++++++++++++++++++++++

Hay muchos programas y aplicaciones que no están incluídos en los repositorios oficiales de *Ubuntu* que, sin embargo, son interesantes y de utilidad. En estos casos se puede utilizar un origen **PPA** *(Personal Package Archives)* que permite gestionar e instalar paquetes mediante las herramientas de **APT**.

Los **PPAs** están alojados en servidores públicos que proveen el servicio. El caso más conocido es *Launchpad* [#]_ que provee un espacio dedicado a los **PPAs** de *Ubuntu* [#]_.

.. [#] `<https://launchpad.net/>`_

.. [#] `<https://launchpad.net/ubuntu/>`_


Para incorporar un **PPA** a como origen de software se debe agregar el repositorio al sistema con el comando :bash:`add-apt-repository`.

En el ejemplo se agrega el repositorio **PPA** del editor de texto *Notepadqq* [#]_.

.. [#] `<http://notepadqq.altervista.org/>`_


.. code-block:: bash

    $ sudo add-apt-repository ppa:notepadqq-team/notepadqq
     Notepadqq text editor
     Más información: https://launchpad.net/~notepadqq-team/+archive/ubuntu/notepadqq
    Pulse [Intro] para continuar o ctrl-c para cancelar
    
    gpg: anillo «/tmp/tmphocgqsxg/secring.gpg» creado
    gpg: anillo «/tmp/tmphocgqsxg/pubring.gpg» creado
    gpg: solicitando clave 63DE9CD4 de hkp servidor keyserver.ubuntu.com
    gpg: /tmp/tmphocgqsxg/trustdb.gpg: se ha creado base de datos de confianza
    gpg: clave 63DE9CD4: clave pública "Launchpad PPA for Notepadqq Team" importada
    gpg: Cantidad total procesada: 1
    gpg:               importadas: 1  (RSA: 1)
    OK

    $ 

Este comando incorpora al anillo de claves de confianza de **APT** la clave con la que se firman los paquetes del **PPA**.

También crea el archivo de origen :bash:`.list` específico del repositorio de software en el directorio :bash:`/etc/apt/sources.list.d/` que será incluído en la siguiente ocasión en que se actualice la base de paquetes disponibles. 

Se puede verificar el contenido del archivo de origen con el comando :bash:`cat`:

.. raw:: pdf

    Spacer 0 10

.. code-block:: bash
    
    $ cat /etc/apt/sources.list.d/notepadqq-team-ubuntu-notepadqq-xenial.list 
    deb http://ppa.launchpad.net/notepadqq-team/notepadqq/ubuntu xenial main
    # deb-src http://ppa.launchpad.net/notepadqq-team/notepadqq/ubuntu xenial main
    
    $ 

.. raw:: pdf

    Spacer 0 10

El paso siguiente es actualizar la base de paquetes disponibles.

.. raw:: pdf

    Spacer 0 10

.. code-block:: bash

    $ sudo apt update
    Obj:1 http://ubuntu.unc.edu.ar/ubuntu xenial InRelease
    Des:2 http://ubuntu.unc.edu.ar/ubuntu xenial-updates InRelease [95,7 kB]                              
    Obj:3 http://ubuntu.unc.edu.ar/ubuntu xenial-backports InRelease                                             
    Des:4 http://ubuntu.unc.edu.ar/ubuntu xenial-security InRelease [94,5 kB]        
    Des:5 http://ppa.launchpad.net/notepadqq-team/notepadqq/ubuntu xenial InRelease 
    [17,5 kB]
    Des:6 http://ppa.launchpad.net/notepadqq-team/notepadqq/ubuntu xenial/main amd64
     Packages [932 B]
    Des:7 http://ppa.launchpad.net/notepadqq-team/notepadqq/ubuntu xenial/main i386 
    Packages [932 B]
    Des:8 http://ppa.launchpad.net/notepadqq-team/notepadqq/ubuntu xenial/main Trans
    lation-en [424 B]
    Descargados 210 kB en 1s (137 kB/s)
    Leyendo lista de paquetes... Hecho
    Creando árbol de dependencias       
    Leyendo la información de estado... Hecho
    Todos los paquetes están actualizados.

    $ 

.. raw:: pdf

    Spacer 0 10

Y el paso final es instalar software.

.. raw:: pdf

    Spacer 0 10

.. code-block:: bash

    $ sudo apt install notepadqq
    Leyendo lista de paquetes... Hecho
    Creando árbol de dependencias       
    Leyendo la información de estado... Hecho
    Se instalarán los siguientes paquetes adicionales:
      libdouble-conversion1v5 libpcre16-3 libqt5core5a libqt5dbus5 libqt5gui5 libqt5
    network5 libqt5opengl5
      libqt5printsupport5 libqt5qml5 libqt5quick5 libqt5sql5 libqt5sql5-sqlite libqt
    5svg5 libqt5webkit5 libqt5widgets5
      libxcb-icccm4 libxcb-image0 libxcb-keysyms1 libxcb-randr0 libxcb-render-util0 
    libxcb-xkb1 libxkbcommon-x11-0
      notepadqq-common qttranslations5-l10n
    Paquetes sugeridos:
      libqt5libqgtk2 qt5-image-formats-plugins qtwayland5
    Se instalarán los siguientes paquetes NUEVOS:
      libdouble-conversion1v5 libpcre16-3 libqt5core5a libqt5dbus5 libqt5gui5 libqt5
    network5 libqt5opengl5
      libqt5printsupport5 libqt5qml5 libqt5quick5 libqt5sql5 libqt5sql5-sqlite libqt
    5svg5 libqt5webkit5 libqt5widgets5
      libxcb-icccm4 libxcb-image0 libxcb-keysyms1 libxcb-randr0 libxcb-render-util0 
    libxcb-xkb1 libxkbcommon-x11-0
      notepadqq notepadqq-common qttranslations5-l10n
    0 actualizados, 25 nuevos se instalarán, 0 para eliminar y 0 no actualizados.
    Se necesita descargar 21,5 MB de archivos.
    Se utilizarán 91,3 MB de espacio de disco adicional después de esta operación.
    ¿Desea continuar? [S/n] 

La ventaja de utilizar **PPAs** reside en que una vez agregado el repositorio al esquema de administración de paquetes de **APT**, se presentarán las actualizaciones de la misma manera que el software instalado desde los orígenes de la distribución.    


Búsqueda de paquetes
++++++++++++++++++++

Una funcionalidad necesaria en la administración de paquetes es la posibilidad de buscar y hacer consultas, tanto de paquetes disponibles para instalar, como aquellos que están instalados. También ver la información asociada a un paquete como sus dependencias, tamaño, funcionalidades, etc.

El parámetro :bash:`search`
~~~~~~~~~~~~~~~~~~~~~~~~~~~

el uso de :bash:`search` permite buscar y mostrar un listado de paquetes asociados a un patrón de expresión regular determinado que esté incluído tanto en el nombre del paquete como en su descripción.

.. code-block:: bash

    $ sudo apt search gedit
    Ordenando... Hecho
    Buscar en todo el texto... Hecho
    debugedit/xenial 4.12.0.1+dfsg1-3build3 amd64
      tool to mangle source locations in .debug files
    
    dgedit/xenial 0~git20151217-2 amd64
      drum kit editor for DrumGizmo
    
    drumgizmo/xenial 0.9.8.1-3 amd64
      Audio sampler plugin and stand-alone app that simulates a real drum kit
    
    gedit/xenial 3.18.3-0ubuntu4 amd64
      Editor de texto oficial del entorno de escritorio GNOME
    
    gedit-common/xenial,xenial 3.18.3-0ubuntu4 all
      Editor de texto oficial del entorno de escritorio GNOME (archivos comunes)
    
    gedit-dev/xenial 3.18.3-0ubuntu4 amd64
      Editor de texto oficial para el entorno del escritorio de GNOME (archivos de d
    esarrollo)
    
    gedit-developer-plugins/xenial,xenial 0.5.15-0ubuntu1 all
      A set of gedit plugins for developers
    
    gedit-latex-plugin/xenial,xenial 3.8.0-3build1 all
      gedit plugin for composing and compiling LaTeX documents
    
    gedit-plugins/xenial 3.18.0-1 amd64
      conjunto de complementos para gedit
    
    gedit-source-code-browser-plugin/xenial,xenial 3.0.3-5 all
      source code class and function browser plugin for Gedit
    
    gigedit/xenial 0.2.0-1build1 amd64
      Editor de instrumentos para archivos Gigasampler
    
    leafpad/xenial 0.8.18.1-4 amd64
      Editor de texto simple basado en GTK+
    
    libgtk2-sourceview2-perl/xenial 0.10-2build2 amd64
      enhanced source code editor widget
    
    libwin-hivex-perl/xenial 1.3.13-1build3 amd64
      Vínculos Perl para hivex
    
    nautilus-admin/xenial,xenial 0.1.5-1 all
      Extension for Nautilus to do administrative operations
    
    python-gtkspellcheck/xenial,xenial 3.0-1.1 all
      spellchecking library written in Python for Gtk based on Enchant
    
    python-gtkspellcheck-doc/xenial,xenial 3.0-1.1 all
      Python GTK Spellcheck common documentation
    
    python3-gtkspellcheck/xenial,xenial 3.0-1.1 all
      spellchecking library written in Python for Gtk based on Enchant
    
    qxgedit/xenial 0.3.0-1 amd64
      MIDI System Exclusive files editor
    
    rabbitvcs-core/xenial,xenial 0.16-1 all
      Easy version control
    
    rabbitvcs-gedit/xenial,xenial 0.16-1 all
      Extensión de Gedit para RabbitVCS
    
    supercollider-gedit/xenial,xenial 1:3.6.6~repack-2-2 all
      SuperCollider mode for Gedit
    
    ~$ 


El parámetro :bash:`show`
~~~~~~~~~~~~~~~~~~~~~~~~~

Para mostrar la información de un paquete, el parámetro indicado es :bash:`show`. Esta información incluye cuáles sus dependencias, tamaño de descarga y de instalación, cuáles son los repositorios desde los que se puede descargar, la descripción del contenido del paquete, una descripción de para qué usar el software, y mucho más.

Conocer estos datos de antemano puede ayudar a tomar la decisión correcta al momento de quitar un paquete o al buscar nuevos para instalar.


.. code-block:: bash

    $ sudo apt show gedit
    
    Package: gedit
    Version: 3.18.3-0ubuntu4
    Priority: optional
    Section: gnome
    Origin: Ubuntu
    Maintainer: Ubuntu Desktop Team <ubuntu-desktop@lists.ubuntu.com>
    Original-Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.aliot
    h.debian.org>
    Bugs: https://bugs.launchpad.net/ubuntu/+filebug
    Installed-Size: 1.774 kB
    Depends: python3:any (>= 3.3.2-2~), libatk1.0-0 (>= 1.12.4), libc6 (>= 2.4), lib
    cairo2 (>= 1.2.4), libenchant1c2a (>= 1.6.0), libgdk-pixbuf2.0-0 (>= 2.22.0), li
    bgirepository-1.0-1 (>= 0.9.3), libglib2.0-0 (>= 2.44), libgtk-3-0 (>= 3.16.2), 
    libgtksourceview-3.0-1 (>= 3.18.0), libpango-1.0-0 (>= 1.14.0), libpeas-1.0-0 (>
    = 1.1.0), libx11-6, libxml2 (>= 2.7.4), gir1.2-gtk-3.0, gir1.2-gtksource-3.0, ge
    dit-common (>= 3.18), gedit-common (<< 3.19), gsettings-desktop-schemas, python3
    -gi (>= 3.0), python3-gi-cairo (>= 3.0), gir1.2-peas-1.0, gir1.2-glib-2.0, gir1.
    2-pango-1.0, libpeas-1.0-0-python3loader, iso-codes
    Recommends: zenity, yelp
    Suggests: gedit-plugins
    Replaces: gedit-common (<< 3.18.1-1ubuntu1)
    Homepage: https://wiki.gnome.org/Apps/Gedit
    Task: ubuntu-desktop, ubuntu-usb, edubuntu-desktop, edubuntu-usb, ubuntu-gnome-d
    esktop, ubuntukylin-desktop
    Supported: 5y
    Download-Size: 406 kB
    APT-Sources: http://ubuntu.unc.edu.ar/ubuntu xenial/main amd64 Packages
    Description: Editor de texto oficial del entorno de escritorio GNOME
     gedit es un editor de texto que tiene la mayoría de las características de
     cualquier editor estándar, extendiendo estas características básicas con
     otras normalmente no encontradas en simples editores de texto. gedit es
     una aplicación gráfica que permite editar varios archivos de texto en una
     ventana (también conocido como pestañas o MDI).
     .
     gedit permite editar texto internacional ya que usa la codificación UTF-8
     de Unicode para los archivos editados. Su conjunto de características
     básicas incluyen resaltado de sintaxis para código fuente, autotabulación,
     impresión y previsualización de la impresión.
     .
     gedit también es ampliable mediante su sistema de extensiones, que
     actualmente permite hacer comprobaciones ortográficas, comparar archivos,
     visualizar los archivos de registro de CVS y ajustar los niveles de
     tabulación.
    
    $ 


El parámetro :bash:`list`
~~~~~~~~~~~~~~~~~~~~~~~~~

La utilidad de este parámetro consiste en mostrar el listado de paquetes que satisfacen cierto criterio de búsqueda. En caso de no indicarle un patrón, el listado será el de todos los paquetes disponibles en los repositorios definidos en el sistema.

.. code-block:: bash

    $ sudo apt list *notepad*
    Listando... Hecho
    notepadqq/xenial,now 0.53.0-0~xenial1 amd64 [instalado]
    notepadqq-common/xenial,xenial,now 0.53.0-0~xenial1 all [instalado, automático]
    notepadqq-gtk/xenial,xenial 0.53.0-0~xenial1 all
    $ 

Los paquetes presentes en el sistema tienen la etiqueta *instalado*, aquellos automáticos tienen su etiqueta correspondiente.

Existen dos parámetros interesantes asociados a :bash:`list`. Uno es :bash:`--installed` que filtra el listado mostrando sólo aquellos paquetes instalados, útil para obtener el listado completo de paquetes del sistema.

.. code-block:: bash

    $ sudo apt list --installed

El otro parámetro asociado a :bash:`list` es, como se muestra en la sección de actualización de paquetes, :bash:`--upgradable`. Que permite visualizar los paquetes disponibles para actualizar luego de ejecutar el comando :bash:`sudo apt update`.


Observaciones
-------------

**APT** tiene más herramientas, algunas de ellas asociadas a :bash:`dpkg` como :bash:`dpkg-query` para búsquedas y consultas o :bash:`dpkg-reconfigure` para volver a ejecutar los pasos de configuración del paquete.

La herramienta :bash:`apt-get` de uso muy similar a :bash:`apt` y con extensiones como :bash:`apt-cache` para realizar consultas, :bash:`apt-add-repository` para la gestión de repositorios, :bash:`apt-key` para gestión de claves, :bash:`apt-mark` para marcar las condiciones de paquetes.

Un recorrido por las páginas de manuales del sistema ofrece una mayor visión sobre la versatilidad de la gestión de software en el sistema operativo.



apt-add-repository    apt-config            apt-get               apt-offline
apt-cache             apt-extracttemplates  apt-key               apt-sortpkgs
apt-cdrom             apt-ftparchive        apt-mark              


.. Cierre de apunte con bibliografía

.. raw:: pdf

    PageBreak

Bibliografía
============

* Páginas de Manual (Man Pages) del sistema operativo.

.. raw:: pdf

    Spacer 0 10

La bibliografía que se indica a continuación corresponde a material que provee una visión interesante sobre los temas propuestos en esta unidad. Es documentación más completa e incluso más extensiva en el desarrollo de algunos temas.

* The Debian Administrator's Handbook, Raphaël Hertzog and Roland Mas, (`https://debian-handbook.info/ <https://debian-handbook.info/>`_)

* `Administración de sistemas GNU/Linux <http://ocw.uoc.edu/informatica-tecnologia-y-multimedia/administracion-de-sistemas-gnu-linux-1>`_, Máster universitario en Software Libre, Universitat Oberta de Catalunya

* Básicamente GNU/Linux, Antonio Perpiñan, Fundación Código Libre Dominicano (`http://www.codigolibre.org <http://www.codigolibre.org>`_)


