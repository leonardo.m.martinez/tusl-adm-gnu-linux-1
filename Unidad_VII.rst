.. header:: 
    Administración de GNU/Linux I - Unidad VII - Configuración de dispositivos - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. role:: bash(code)
    :language: bash

.. Agregamos la Carátula definida en el estilo

.. Activamos el numerado de las secciones
.. sectnum::

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
    PageBreak


Copyright © 2016.

:Autor:       Leonardo Martinez  

.. 
    :Colaborador: 
    :Colaborador: 
    :Colaborador: 

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución Compartir Igual (CC-BY-SA) 4.0 Internacional. `<http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/licencia.png 

.. raw:: pdf

    PageBreak 
..    SetPageCounter 1 arabic


Configuración de dispositivos
=============================

Dentro de las tareas del administrador se encuentra la de gestionar los dispositivos conectados a las computadoras. En el caso de las distribuciones de *GNU/Linux* estas tareas se realizan con una serie de comandos y aplicaciones que permiten identificar y configurar los dispositivos conectados.

Entre los dispositivos comunes podemos encontrar el mouse, el teclado, el monitor, un dispositivo de almacenamiento externo (pendrives, discos, tarjetas de memoria), un escáner o una impresora.

Con excepción de escáneres e impresoras, los demás dispositivos, en su mayoría, son detectados por el sistema operativo y puestos a disposión del usuario en forma automática.

Obtener información del dispositivo
-----------------------------------

Para obtener información acerca de los dispositivos conectados al sistema se utilizan una los comandos :bash:`lspci` y :bash:`lsusb`.

El comando :bash:`lspci`
++++++++++++++++++++++++

Este comando se utiliza para obtener la información de los canales **PCI** y los dispositivos conectados a los mismos. Por defecto muestra una lista con información resumida, pero su puede ampliar utilizando los parámetros :bash:`-v` y :bash:`-vv`.

El comando ejecutado en una máquina virtual en *VirtualBox* muestra lo siguiente:

.. code-block:: bash

    $ lspci
    00:00.0 Host bridge: Intel Corporation 440FX - 82441FX PMC [Natoma] (rev 02)
    00:01.0 ISA bridge: Intel Corporation 82371SB PIIX3 ISA [Natoma/Triton II]
    00:01.1 IDE interface: Intel Corporation 82371AB/EB/MB PIIX4 IDE (rev 01)
    00:02.0 VGA compatible controller: InnoTek Systemberatung GmbH VirtualBox Graphi
    cs Adapter
    00:03.0 Ethernet controller: Intel Corporation 82540EM Gigabit Ethernet Controll
    er (rev 02)
    00:04.0 System peripheral: InnoTek Systemberatung GmbH VirtualBox Guest Service
    00:05.0 Multimedia audio controller: Intel Corporation 82801AA AC'97 Audio Contr
    oller (rev 01)
    00:06.0 USB controller: Apple Inc. KeyLargo/Intrepid USB
    00:07.0 Bridge: Intel Corporation 82371AB/EB/MB PIIX4 ACPI (rev 08)
    00:0b.0 USB controller: Intel Corporation 82801FB/FBM/FR/FW/FRW (ICH6 Family) US
    B2 EHCI Controller
    00:0d.0 SATA controller: Intel Corporation 82801HM/HEM (ICH8M/ICH8M-E) SATA Cont
    roller [AHCI mode] (rev 02)

En un equipo físico, la salida del comando muestra:


.. code-block:: bash

    $ lspci
    00:00.0 Host bridge: Intel Corporation Mobile PM965/GM965/GL960 Memory Controlle
    r Hub (rev 03)
    00:02.0 VGA compatible controller: Intel Corporation Mobile GM965/GL960 Integrat
    ed Graphics Controller (primary) (rev 03)
    00:02.1 Display controller: Intel Corporation Mobile GM965/GL960 Integrated Grap
    hics Controller (secondary) (rev 03)
    00:1a.0 USB controller: Intel Corporation 82801H (ICH8 Family) USB UHCI Controll
    er #4 (rev 04)
    00:1a.1 USB controller: Intel Corporation 82801H (ICH8 Family) USB UHCI Controll
    er #5 (rev 04)
    00:1a.7 USB controller: Intel Corporation 82801H (ICH8 Family) USB2 EHCI Control
    ler #2 (rev 04)
    00:1b.0 Audio device: Intel Corporation 82801H (ICH8 Family) HD Audio Controller
     (rev 04)
    00:1c.0 PCI bridge: Intel Corporation 82801H (ICH8 Family) PCI Express Port 1 (r
    ev 04)
    00:1c.3 PCI bridge: Intel Corporation 82801H (ICH8 Family) PCI Express Port 4 (r
    ev 04)
    00:1c.5 PCI bridge: Intel Corporation 82801H (ICH8 Family) PCI Express Port 6 (r
    ev 04)
    00:1d.0 USB controller: Intel Corporation 82801H (ICH8 Family) USB UHCI Controll
    er #1 (rev 04)
    00:1d.1 USB controller: Intel Corporation 82801H (ICH8 Family) USB UHCI Controll
    er #2 (rev 04)
    00:1d.2 USB controller: Intel Corporation 82801H (ICH8 Family) USB UHCI Controll
    er #3 (rev 04)
    00:1d.7 USB controller: Intel Corporation 82801H (ICH8 Family) USB2 EHCI Control
    ler #1 (rev 04)
    00:1e.0 PCI bridge: Intel Corporation 82801 Mobile PCI Bridge (rev f4)
    00:1f.0 ISA bridge: Intel Corporation 82801HM (ICH8M) LPC Interface Controller (
    rev 04)
    00:1f.1 IDE interface: Intel Corporation 82801HM/HEM (ICH8M/ICH8M-E) IDE Control
    ler (rev 04)
    00:1f.2 SATA controller: Intel Corporation 82801HM/HEM (ICH8M/ICH8M-E) SATA Cont
    roller [AHCI mode] (rev 04)
    00:1f.3 SMBus: Intel Corporation 82801H (ICH8 Family) SMBus Controller (rev 04)
    06:00.0 Network controller: Intel Corporation PRO/Wireless 3945ABG [Golan] Netwo
    rk Connection (rev 02)
    08:00.0 Ethernet controller: Broadcom Corporation NetLink BCM5787M Gigabit Ether
    net PCI Express (rev 02)
    0a:09.0 FireWire (IEEE 1394): Ricoh Co Ltd R5C832 IEEE 1394 Controller (rev 05)
    0a:09.1 SD Host controller: Ricoh Co Ltd R5C822 SD/SDIO/MMC/MS/MSPro Host Adapte
    r (rev 22)
    0a:09.2 System peripheral: Ricoh Co Ltd R5C592 Memory Stick Bus Host Adapter (re
    v 12)
    0a:09.3 System peripheral: Ricoh Co Ltd xD-Picture Card Controller (rev 12)
    
    $ 


El uso del parámetro :bash:`-mm` muestra la información de salida del comando preparada para ser procesada por un *shell script*.

.. code-block:: bash

    $ lspci -mm
    00:00.0 "Host bridge" "Intel Corporation" "440FX - 82441FX PMC [Natoma]" -r02 ""
     ""
    00:01.0 "ISA bridge" "Intel Corporation" "82371SB PIIX3 ISA [Natoma/Triton II]" 
    "" ""
    00:01.1 "IDE interface" "Intel Corporation" "82371AB/EB/MB PIIX4 IDE" -r01 -p8a 
    "" ""
    00:02.0 "VGA compatible controller" "InnoTek Systemberatung GmbH" "VirtualBox Gr
    aphics Adapter" "" ""
    00:03.0 "Ethernet controller" "Intel Corporation" "82540EM Gigabit Ethernet Cont
    roller" -r02 "Intel Corporation" "PRO/1000  MT Desktop Adapter"
    00:04.0 "System peripheral" "InnoTek Systemberatung GmbH" "VirtualBox Guest Serv
    ice" "" ""
    00:05.0 "Multimedia audio controller" "Intel Corporation" "82801AA AC'97 Audio C
    ontroller" -r01 "Dell" "82801AA AC'97 Audio     Controller"
    00:06.0 "USB controller" "Apple Inc." "KeyLargo/Intrepid USB" -p10 "" ""
    00:07.0 "Bridge" "Intel Corporation" "82371AB/EB/MB PIIX4 ACPI" -r08 "" ""
    00:0b.0 "USB controller" "Intel Corporation" "82801FB/FBM/FR/FW/FRW (ICH6 Family
    ) USB2 EHCI Controller" -p20 "" ""
    00:0d.0 "SATA controller" "Intel Corporation" "82801HM/HEM (ICH8M/ICH8M-E) SATA 
    Controller [AHCI mode]" -r02 -p01 "" ""
    
    $

.. raw:: pdf

    Spacer 0 5

Obteniendo más información con el comando :bash:`-v`:
    

.. code-block:: bash

    $ lspci -v
    00:00.0 Host bridge: Intel Corporation 440FX - 82441FX PMC [Natoma] (rev 02)
    	Flags: fast devsel
    
    00:01.0 ISA bridge: Intel Corporation 82371SB PIIX3 ISA [Natoma/Triton II]
    	Flags: bus master, medium devsel, latency 0
    
    00:01.1 IDE interface: Intel Corporation 82371AB/EB/MB PIIX4 IDE (rev 01) (prog-
    if 8a [Master SecP PriP])
    	Flags: bus master, fast devsel, latency 64
    	[virtual] Memory at 000001f0 (32-bit, non-prefetchable) [size=8]
    	[virtual] Memory at 000003f0 (type 3, non-prefetchable)
    	[virtual] Memory at 00000170 (32-bit, non-prefetchable) [size=8]
    	[virtual] Memory at 00000370 (type 3, non-prefetchable)
    	I/O ports at d000 [size=16]
    	Kernel driver in use: ata_piix
    	Kernel modules: pata_acpi
    
    00:02.0 VGA compatible controller: InnoTek Systemberatung GmbH VirtualBox Graphi
    cs Adapter (prog-if 00 [VGA controller])
    	Flags: bus master, fast devsel, latency 0, IRQ 18
    	Memory at e0000000 (32-bit, prefetchable) [size=128M]
    	Expansion ROM at <unassigned> [disabled]
    	Kernel driver in use: vboxvideo
    	Kernel modules: vboxvideo
    
    00:03.0 Ethernet controller: Intel Corporation 82540EM Gigabit Ethernet Controll
    er (rev 02)
    	Subsystem: Intel Corporation PRO/1000 MT Desktop Adapter
    	Flags: bus master, 66MHz, medium devsel, latency 64, IRQ 19
    	Memory at f0000000 (32-bit, non-prefetchable) [size=128K]
    	I/O ports at d010 [size=8]
    	Capabilities: <access denied>
    	Kernel driver in use: e1000
    	Kernel modules: e1000
    
    00:04.0 System peripheral: InnoTek Systemberatung GmbH VirtualBox Guest Service
    	Flags: bus master, fast devsel, latency 0, IRQ 20
    	I/O ports at d020 [size=32]
    	Memory at f0400000 (32-bit, non-prefetchable) [size=4M]
    	Memory at f0800000 (32-bit, prefetchable) [size=16K]
    	Kernel driver in use: vboxguest
    	Kernel modules: vboxguest
    
    00:05.0 Multimedia audio controller: Intel Corporation 82801AA AC'97 Audio Contr
    oller (rev 01)
    	Subsystem: Dell 82801AA AC'97 Audio Controller
    	Flags: bus master, medium devsel, latency 64, IRQ 21
    	I/O ports at d100 [size=256]
    	I/O ports at d200 [size=64]
    	Kernel driver in use: snd_intel8x0
    	Kernel modules: snd_intel8x0
    
    00:06.0 USB controller: Apple Inc. KeyLargo/Intrepid USB (prog-if 10 [OHCI])
    	Flags: bus master, fast devsel, latency 64, IRQ 22
    	Memory at f0804000 (32-bit, non-prefetchable) [size=4K]
    	Kernel driver in use: ohci-pci
    
    00:07.0 Bridge: Intel Corporation 82371AB/EB/MB PIIX4 ACPI (rev 08)
    	Flags: bus master, medium devsel, latency 0, IRQ 9
    	Kernel driver in use: piix4_smbus
    	Kernel modules: i2c_piix4
    
    00:0b.0 USB controller: Intel Corporation 82801FB/FBM/FR/FW/FRW (ICH6 Family) US
    B2 EHCI Controller (prog-if 20 [EHCI])
    	Flags: bus master, fast devsel, latency 64, IRQ 19
    	Memory at f0805000 (32-bit, non-prefetchable) [size=4K]
    	Kernel driver in use: ehci-pci
    
    00:0d.0 SATA controller: Intel Corporation 82801HM/HEM (ICH8M/ICH8M-E) SATA Cont
    roller [AHCI mode] (rev 02) (prog-if 01     [AHCI 1.0])
    	Flags: bus master, fast devsel, latency 64, IRQ 21
    	I/O ports at d240 [size=8]
    	I/O ports at 0000
    	I/O ports at d250 [size=8]
    	I/O ports at 0000
    	I/O ports at d260 [size=16]
    	Memory at f0806000 (32-bit, non-prefetchable) [size=8K]
    	Capabilities: <access denied>
    	Kernel driver in use: ahci
    	Kernel modules: ahci
    
    $ 

.. raw:: pdf

    Spacer 0 5
    
Para obtener información detallada y listada de manera que se pueda utilizar en un *shell script* para procesarla se puede utilizar una combinación de estos parámetros como :bash:`-vmm`.

.. code-block:: bash

    $ lspci -vmm
    Slot:	00:00.0
    Class:	Host bridge
    Vendor:	Intel Corporation
    Device:	440FX - 82441FX PMC [Natoma]
    Rev:	02

    Slot:	00:01.0
    Class:	ISA bridge
    Vendor:	Intel Corporation
    Device:	82371SB PIIX3 ISA [Natoma/Triton II]
    
    Slot:	00:01.1
    Class:	IDE interface
    Vendor:	Intel Corporation
    Device:	82371AB/EB/MB PIIX4 IDE
    Rev:	01
    ProgIf:	8a
    
    Slot:	00:02.0
    Class:	VGA compatible controller
    Vendor:	InnoTek Systemberatung GmbH
    Device:	VirtualBox Graphics Adapter
    
    Slot:	00:03.0
    Class:	Ethernet controller
    Vendor:	Intel Corporation
    Device:	82540EM Gigabit Ethernet Controller
    SVendor:	Intel Corporation
    SDevice:	PRO/1000 MT Desktop Adapter
    Rev:	02
    
    Slot:	00:04.0
    Class:	System peripheral
    Vendor:	InnoTek Systemberatung GmbH
    Device:	VirtualBox Guest Service
        
    Slot:	00:05.0
    Class:	Multimedia audio controller
    Vendor:	Intel Corporation
    Device:	82801AA AC'97 Audio Controller
    SVendor:	Dell
    SDevice:	82801AA AC'97 Audio Controller
    Rev:	01
    
    Slot:	00:06.0
    Class:	USB controller
    Vendor:	Apple Inc.
    Device:	KeyLargo/Intrepid USB
    ProgIf:	10
    
    Slot:	00:07.0
    Class:	Bridge
    Vendor:	Intel Corporation
    Device:	82371AB/EB/MB PIIX4 ACPI
    Rev:	08
    
    Slot:	00:0b.0
    Class:	USB controller
    Vendor:	Intel Corporation
    Device:	82801FB/FBM/FR/FW/FRW (ICH6 Family) USB2 EHCI Controller
    ProgIf:	20
    
    Slot:	00:0d.0
    Class:	SATA controller
    Vendor:	Intel Corporation
    Device:	82801HM/HEM (ICH8M/ICH8M-E) SATA Controller [AHCI mode]
    Rev:	02
    ProgIf:	01

    
Las etiquetas de los campos se describen en la página de manual del comando.


El comando :bash:`lsusb`
++++++++++++++++++++++++

Este comando se utiliza para obtener la información de los canales **USB** y los dispositivos conectados a los mismos. Por defecto muestra una lista con información resumida, pero su puede ampliar utilizando el parámetro :bash:`-v`.

En una máquina virtual muestra la siguiente información, donde el dispositivo *VirtualBox USB Tablet* corresponde al dispositivo táctil de la pantalla:

.. code-block:: bash

    administrador@xubuntu1604:~$ lsusb 
    Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
    Bus 002 Device 002: ID 80ee:0021 VirtualBox USB Tablet
    Bus 002 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
    administrador@xubuntu1604:~$ 

.. raw:: pdf

    Spacer 0 5

En un equipo físico se obtuvo la información asociada a un disco externo en el *dispositivo 006* del *canal 002*, el conector bluetooth del mouse inalámbrico en el *dispositivo 003* del *canal 005* y el dispositivo de la cámara web en el *dispositivo 002* del *canal 001*.

.. code-block:: bash

    $ lsusb 
    Bus 002 Device 006: ID 1058:1023 Western Digital Technologies, Inc. Elements SE 
    Portable (WDBABV)
    Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
    Bus 007 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
    Bus 006 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
    Bus 005 Device 003: ID 045e:0745 Microsoft Corp. Nano Transceiver v1.0 for Blue
    tooth
    Bus 005 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
    Bus 001 Device 002: ID 064e:a103 Suyin Corp. Acer/HP Integrated Webcam [CN0314]
    Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
    Bus 004 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
    Bus 003 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
    $ 

Para ver la información específica de un dispositivo se utiliza el parámetro :bash:`-D` e indicando el dispositivo.

.. code-block:: bash

    $ lsusb -D /dev/bus/usb/001/002 
    Device: ID 064e:a103 Suyin Corp. Acer/HP Integrated Webcam [CN0314]
    Couldn't open device, some information will be missing
    Device Descriptor:
      bLength                18
      bDescriptorType         1
      bcdUSB               2.00
      bDeviceClass          239 Miscellaneous Device
      bDeviceSubClass         2 ?
      bDeviceProtocol         1 Interface Association
      bMaxPacketSize0        64
      idVendor           0x064e Suyin Corp.
      idProduct          0xa103 Acer/HP Integrated Webcam [CN0314]
      bcdDevice            1.00
      iManufacturer           2 
      iProduct                1 
      iSerial                 3 
      bNumConfigurations      1

    [...]

    leonardo@albion:~$

Se debe tener en cuenta que este comando muestra mucha información acerca del dispositivo.
    
Configuración de impresoras
---------------------------

La configuración de impresión solía causar dolores de cabeza tanto a administradores como a usuarios. Estos problemas son ahora algo del pasado gracias a la creación de **CUPS**, el servidor de impresión libre que utiliza el protocolo *IPP* (Internet Printing Protocol).

**CUPS** [#]_ (Common Unix Printing System) es un proyecto, y también una marca registrada, administrado por Apple, Inc. 

.. [#] http://www.cups.org/

Este programa está dividido en varios paquetes *Debian*: :bash:`cups` es el servidor de impresión central; :bash:`cups-bsd` es una capa de compatibilidad que permite utilizar los programas del sistema de impresión *BSD* tradicional (el demonio :bash:`lpd`, los programas :bash:`lpr` y :bash:`lpq`, etc.); :bash:`cups-client` que contiene un grupo de programas para interactuar con el servidor (bloquear o desbloquear una impresora, ver o eliminar trabajos de impresión en curso, etc.); y por último :bash:`cups-driver-gutenprint` contiene una colección de controladores de impresión adicionales para cups.

Luego de instalar estos paquetes, se puede administrar **CUPS** fácilmente a través de una interfaz web en la dirección local: https://localhost:631/ , desde donde es posible agregar impresoras, incluyendo impresoras de red, eliminarlas y administrarlas.

La mayoría de las distribuciones *GNU/Linux* poseen aplicaciones gráficas para instalar y administrar las impresoras. Sin embargo, como se viene exponiendo en el curso, todas la tareas de administración pueden realizarse desde la línea de comandos. La configuración del servicio de impresión no es una excepción.

Para ver el listado de dispositivos de impresión configurados en el sistema, el comando a utilizar es:

.. code-block:: bash

    $ lpstat -a
    lpstat: No se han añadido destinos.
    administrador@xubuntu1604:~$ 

Al no encontrar impresoras definidas en el sistema, el comando advierte que no hay destinos para los trabajos de impresión.

Para ver las impresoras conectadas y/o disponibles en el sistema se utiliza el comando :bash:`lpinfo -v`. El comando presenta un listado de los dispositivos disponibles de acuedo a los protocolos soportados.

.. code-block:: bash

    $ lpinfo -v
    network beh
    direct hp
    network ipps
    network https
    network ipp
    network http
    network lpd
    network ipp14
    network smb
    network socket
    direct hpfax
    network dnssd://Photosmart%20Plus%20B210%20series%20%5B21FC9D%5D._pdl-datastream
        ._tcp.local/
    network socket://192.168.0.25:9100
    
    $

En este caso se observa la existencia de un dispositivo accesible mediante la *URI* :bash:`dnssd://Photosmart%20Plus%20B210%20series%20%5B21FC9D%5D._pdl-datastream._tcp.local/` y otro mediante la *URI* :bash:`socket://192.168.0.25:9100`.

En este caso, se trata de la misma impresora a la que se puede acceder mediante dos protocolos diferentes.

Para agregar la impresora al sistema, el comando de **CUPS** para esta tarea es :bash:`lpadmin`.

.. code-block:: bash

    $ sudo lpadmin -p HPB210a -E -v socket://192.168.0.25:9100

Con el parámetro :bash:`-p` se indica el nombre de la impresora, con el parámetro :bash:`-E` se habilita la impresora para su uso inmediatamente después de haber sido creada. Y por último, el parámetro :bash:`-v` determina el atributo de *URI* del dispositivo utilizando alguno de los valores de la salida del comando :bash:`lpinfo`.

Para definir la impresora como predeterminada del sistema se utiliza el parámetro :bash:`-d`:

.. code-block:: bash

    $ sudo lpadmin -d HPB210a
    $

Para comprobar que la impresora esté configurada como predeterminada y aceptando trabajos de impresión, se ejecutan los comandos:

.. code-block:: bash

    $ lpstat -a
    HPB210a aceptando peticiones desde sáb 01 oct 2016 01:56:25 ART
    
    $ lpstat -d
    destino predeterminado del sistema: HPB210a

    $

Elegir una impresora
++++++++++++++++++++

Al momento de elegir la impresora es importante conocer sus características y funciones, pero sobre todo la compatibilidad con *GNU/Linux*.

La mayoría de los fabricantes proporcionan los *drivers* para *GNU/Linux* de los modelos de impresoras que proveen.

Una base de consulta sobre la compatibilidad de impresoras es el sitio `Open Printing <http://www.openprinting.org/printers>`_ [#]_ que tiene una base de datos donde muestra el nivel de compatibilidad de impresoras filtrado por marcas y modelos.

.. [#] http://www.openprinting.org/printers


Interfaz web de CUPS
++++++++++++++++++++

Para administrar el servicio de impresión, **CUPS** posee una interfaz web en la dirección local https://localhost:631/, accesible desde cualquier navegador. La pantalla inicial es la siguiente: 

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/cupsweb01.png 
   :width: 15cm

.. raw:: pdf

    Spacer 0 20

La sección de *Administración* es donde se realiza toda la gestión de impresoras y sus trabajos de impresión.

.. raw:: pdf

    PageBreak

El área *Server* permite gestionar todo lo relacionado con el servicio de impresión como editar el archivo de configuración y revisar los archivos de *logs* del servicio.

En el área *Printers* se ubican los accesos para la gestión de las impresoras, las clases de impresoras y los trabajos de impresión.

Para agregar una impresora al sistema se debe presionar en el botón **Add Printer**. 

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/cupsweb02.png 
   :width: 15cm

.. raw:: pdf

    Spacer 0 20

En la pantalla siguiente se muestra un listado de las opciones para incorporar una impresora local, de red o aquellas descubiertas automáticamentes en la red.

Los protocolos de red disponibles son:

* Internet Printing Protocol (ipp, ipp14, ipps, http, https) provisto por otro servicio **CUPS** alojado en otro equipo de la red.
* LDP/LDR, servicios de impresión en servidores *\*nix* o directamente provistos por el software embebido en las impresoras.
* AppSocket/HP JetDirect, es un protocolo que se escucha en el puerto 9100, es el más simple y recibe la información en formato crudo o *raw* mediante *PCL* o *PS*.
* Windows Printer via SAMBA. Este es el protocolo de los sistemas Windows para comapartir recursos mediante el protocolo *CIFS*. 


.. raw:: pdf

    Spacer 0 20

Para configurar la misma impresora del ejemplo de línea de comando se selecciona la opción *Photosmart Plus B210a (HP Photosmart Plus B210 series)*.


.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/cupsweb03.png 
   :width: 12cm

.. raw:: pdf

    Spacer 0 20

Se cambia el nombre de la impresora para poder identificarla mejor cuando se realicen impresiones desde las aplicaciones del sistema. En este caso se utiliza **HPB210a**.

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/cupsweb04.png 
   :width: 12cm

.. raw:: pdf

    Spacer 0 20

Al seleccionar inicialmente una impresora descubierta en la red por **CUPS**, algunos de los parámetros de configuración son definidos por la información que presenta la misma impresora, uno de ellos es la marca. Por lo tanto, en la siguiente pantalla sólo se debe indicar el modelo de impresora. Generalmente, **CUPS** preselecciona el más adecuado.

.. raw:: pdf

    Spacer 0 10

.. image:: imagenes/cupsweb05.png 
   :width: 12cm

.. raw:: pdf

    Spacer 0 10

Luego de agregar la impresora el último paso antes de que esté disponible es configurar los parámetros generales.

.. raw:: pdf

    Spacer 0 10

.. image:: imagenes/cupsweb06.png 
   :width: 12cm

.. raw:: pdf

    Spacer 0 10

Una vez que la impresora está lista y disponible para utilizar, se pueden realizar tareas de mantenimiento y administración desde la sección *Printers* seleccionando la impresora.

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/cupsweb07.png 
   :width: 12cm

.. raw:: pdf

    Spacer 0 20

Desde el menú desplegable de *Maintenance* se puede imprimir una página de prueba, pausar la impresora y rechazar, cancelar o mover a otra impresora los trabajos encolados. 

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/cupsweb08.png 
   :width: 12cm

.. raw:: pdf

    Spacer 0 20

Desde el menú desplegable de *Administration* se puede modificar o borrar la impresora, seleccionarla como impresora por defecto del servidor, modificar las opciones o definir los usuarios que pueden utilizarla. 

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/cupsweb09.png 
   :width: 12cm

.. raw:: pdf

    Spacer 0 20

Luego de definir la impresora como opción por defecto del servidor, la información asociada a la impresora queda de la siguiente manera:

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/cupsweb10.png 
   :width: 12cm

.. raw:: pdf

    Spacer 0 20



Interfaz gráfica en Xubuntu
+++++++++++++++++++++++++++

Otra opción gráfica para definir y configurar la impresora es la herramienta *system-config-printer* accesible desde el menú de aplicaciones, en la sección *Configuración*, y ejecutando el programa *Impresoras*.

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/gnomeprinter01.png 
   :width: 8cm

.. raw:: pdf

    Spacer 0 20

Esta aplicación permite agregar impresoras presionando el botón *Añadir*

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/gnomeprinter02.png 
   :width: 8cm

.. raw:: pdf

    PageBreak

La siguiente pantalla presenta el mismo listado de tipo de dispositivo que muestra **CUPS**.

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/gnomeprinter03.png 
   :width: 10cm

.. raw:: pdf

    Spacer 0 20

Al seleccionar la impresora *HP Photosmart Plus B210 (192.168.0.25)* solicita elegir el tipo de conexión.

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/gnomeprinter04.png 
   :width: 10cm

.. raw:: pdf

    Spacer 0 20

En este caso se selecciona la conexión *HP Linux Imaging and Printing (HPLIP)*.

Luego en la pantalla de descripción de la impresora, se indica el nombre para identificarla mejor cuando se realicen impresiones desde las aplicaciones del sistema. En este caso se utiliza **HPB210a**.

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/gnomeprinter05.png 
   :width: 12cm

.. raw:: pdf

    Spacer 0 20

Como último paso en la instalación y configuración de la impresora, se puede imprimir una página de prueba para verificar el funcionamiento correcto.


.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/gnomeprinter06.png 
   :width: 10cm

.. raw:: pdf

    Spacer 0 20

Una vez finalizada la configuración, se dispone del acceso a la impresora para realizar las tareas de administractión y verificar el estado de los trabajos encolados. El dibujo de la tilde verde indica la impresora definida por defecto en el sistema.

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/gnomeprinter07.png 
   :width: 10cm

.. raw:: pdf

    Spacer 0 20


Configuración de escáneres
--------------------------

La configuración de escáneres es un poco más simple que las impresoras dependiendo de la marca, el modelo y también de la conexión utilizada. 

En el ejemplo de la impresora multifunción descripta, la funcionalidad de scanner se obtiene mediante el *backend HPLIP*. Al ejecutar el comando par detectar los dispositivos disponibles se obtiene:

.. raw:: pdf

    Spacer 0 5

.. code-block:: bash

    $ scanimage -L    
    device `hpaio:/net/Photosmart_Plus_B210_series?zc=HP21FC9D' is a Hewlett-Packard
     Photosmart_Plus_B210_series all-in-one

    $

.. raw:: pdf

    Spacer 0 5

En este caso, al utilizar un programa de escaneo, la disponibilidad es automática.

El mismo comando :bash:`scanimage` permite realizar escaneos, es necesario indicarle los parámetros para la obtención de la imagen. El formato de salida puede ser *pnm* o *tiff*.

En ejemplo de ejecución en color con una resolución de 600 ppp:

.. raw:: pdf

    Spacer 0 5

.. code-block:: bash

    $ scanimage --mode Color --resolution 600 > imagen.pnm

.. raw:: pdf

    Spacer 0 5

Para ver el resto de los parámetros y la combinación de opciones se debe consultar la página del manual del comando.

El software *Simple Scan* es una aplicación simple, como lo indica su nombre, y permite realizar escaneos desde los dispositivos conectados al sistema.

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/simplescan_menu.png 
   :width: 12cm

.. raw:: pdf

    Spacer 0 20


Al seleccionar la opción *Propiedades* en el menú, se accede a la ventana donde se pueden definir las opciones de escaneo y seleccionar el dispositivo origen en el caso de que hubiera más de uno.


.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/simplescan_pref.png 
   :width: 8cm

.. raw:: pdf

    PageBreak



Instalación de un escáner USB
+++++++++++++++++++++++++++++

El sistema operativo detecta automáticamente a la mayoría de los escáneres *USB*. Mientras el *backend* esté habilitado, no necesitan mayor configuración. Sin embargo, si el sistema no detecta en forma automática el escáner, éste funciona en forma errática o requiere de una configuración determinada, se debe especificar esa información en el archivo de configuración del *backend* del escáner.

Para ver la compatibilidad de los escáneres con el software **SANE** se puede explorar la página web del programa en la sección de dispositivos soportados [#]_.

.. [#] http://sane-project.org/sane-supported-devices.html


Para ello se deben realizar una serie de pasos. Primero, con el escáner encendido y con el cable *USB* conectado a la computadora, ejecutar el comando:

.. code-block:: bash

    $ sane-find-scanner

La salida de este comando debería ser algo como lo siguiente:

.. code-block:: bash

      # sane-find-scanner will now attempt to detect your scanner. If the
      # result is different from what you expected, first make sure your
      # scanner is powered up and properly connected to your computer.

      # No SCSI scanners found. If you expected something different, make sure that
      # you have loaded a kernel SCSI driver for your SCSI adapter.

    found USB scanner (vendor=0x123a [EXAMPLE SCANNER], product=0x2089 [Example Scan
    ner]) at libusb:001:003

      # Your USB scanner was (probably) detected. It may or may not be supported by
      # SANE. Try scanimage -L and read the backend's manpage.
    
      # Not checking for parallel port scanners.

      # Most Scanners connected to the parallel port or other proprietary ports
      # can't be detected by this program.


De todo ese texto, la línea importante es:

.. code-block:: bash

    found USB scanner (vendor=0x123a [EXAMPLE SCANNER], product=0x2089 [Example Scan
    ner]) at libusb:001:003

De esta línea se necesitan los valores de :bash:`vendor`, en este caso :bash:`0x123a`, y :bash:`product`, en este ejemplo :bash:`0x2089`.

Para identificar el archivo de configuración de *backend*, se puede consultar la página de búsqueda de dispositivos soportados del software **SANE** [#]_.

.. [#] http://www.sane-project.org/cgi-bin/driver.pl

Estos valores se deben agregar al archivo de configuración del *backend* ubicado en el directorio :bash:`/etc/sane.d/`.

Dentro del archivo, localizar la línea que contiene el comando :bash:`usb` y agregarle los valores mostrados por el comando :bash:`sane-find-scanner` quedando la línea de configuración así:

.. code-block:: bash

    usb 0x123a 0x2089

.. Cierre de apunte con bibliografía


Bibliografía
============

* Páginas de Manual (Man Pages) del sistema operativo.

.. raw:: pdf

    Spacer 0 10

La bibliografía que se indica a continuación corresponde a material que provee una visión interesante sobre los temas propuestos en esta unidad. Es documentación más completa e incluso más extensiva en el desarrollo de algunos temas.

* The Debian Administrator's Handbook, Raphaël Hertzog and Roland Mas, (`https://debian-handbook.info/ <https://debian-handbook.info/>`_)

* `Administración de sistemas GNU/Linux <http://ocw.uoc.edu/informatica-tecnologia-y-multimedia/administracion-de-sistemas-gnu-linux-1>`_, Máster universitario en Software Libre, Universitat Oberta de Catalunya

* Básicamente GNU/Linux, Antonio Perpiñan, Fundación Código Libre Dominicano (`http://www.codigolibre.org <http://www.codigolibre.org>`_)


