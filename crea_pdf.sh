#!/bin/bash

#rst2pdf ${1}.rst ../ADM-GNU-LINUX-I/${1}.pdf -s estilos/tusl.style --repeat-table-rows -e preprocess
#rst2pdf ${1}.rst ../ADM-GNU-LINUX-I/${1}.pdf -s estilos_propios/tusl2.style --repeat-table-rows -e preprocess

DIRECTORIO="../TUSL_ADMIN_GNULINUX_I_PDF"

if [[ ! -d ${DIRECTORIO} ]]; then
    mkdir $DIRECTORIO
fi

for ARCHIVO in `ls *.rst`; do
    echo -ne "\nCreando -> ${ARCHIVO%.*}.PDF\n"
    rst2pdf ${ARCHIVO} ${DIRECTORIO}/${ARCHIVO%.*}.pdf -s estilos_propios/tusl2.yaml --repeat-table-rows -e preprocess
done