=======================
Unidad I - Cuestionario
=======================

**Pregunta 1 (C):** Las herramientas gráficas de administración ...

A) ...son siempre aplicaciones *front-end* que ejecutan en segundo plano los mismos comandos disponibles en la *línea de comando*.
B) ...son implementaciones que no tienen relación con los comandos disponibles en la *línea de comando*.
C) ...son, generalmente, aplicaciones *front-end* que ejecutan en segundo plano los mismos comandos disponibles en la *línea de comando*. 

.. raw:: pdf

    Spacer 0 15

**Pregunta 2 (A):** El objetivo de la *LSB* es desarrollar y promover ... 

A) ...un conjunto de estándares que aumentarán la compatibilidad entre las distribuciones de *GNU/Linux*.
B) ...la interoperatividad de las distribuciones *GNU/Linux*.
C) ...un conjunto de estándares exclusivo de las distribuciones basadas en *RedHat*.

.. raw:: pdf

    Spacer 0 15

**Pregunta 3 (C):** ¿Cuál es la distribución de *GNU/Linux* que tiene críticas a la *LSB* y retiró su apoyo en 2015? 

A) *ArchLinux*
B) *Redhat*
C) *Debian*

.. raw:: pdf

    Spacer 0 15

**Pregunta 4 (C):** El administrador del sistema es responsable de asegurar que el sistema **GNU/Linux** brinde los servicios necesarios para que los usuarios puedan cumplir con sus tareas. Esto involucra una serie de actividades de suma importancia.

*Seleccionar tres correctas (A, D, F, G)*:
 
A) Agregar nuevos usuarios al sistema y configurar sus directorios personales y los privilegios básicos.
B) Editar y crear documentos de texto y planillas de cálculo siguiendo las instrucciones del usuario.
C) Instalar nuevos componentes de hardware y verificar la correcta instalación del sistema BIOS.
D) Realizar cableados de instalaciones o reparar cables.
E) Distribuir los listados impresos en las oficinas de los usuarios.
F) Evaluar económicamente compras de equipamiento de hardware y software.
G) Evitar el abuso de tareas automáticas para i el rendimiento general del trabajo en su organización.

.. raw:: pdf

    Spacer 0 15

**Pregunta 5 (B):** La mayoría de las distribuciones de **GNU/Linux** tienen un proceso instalación my bien elaborado e intuitivo, ...

A) ...se realiza sin intervención, de forma totalmente automática.
B) ...basta con tener definido que tipo de servicios va a prestar el equipo y las necesidades del usuario que va a utilizar el equipo. 
C) ...sin embargo, hay que tener bien definida la lista de usuarios que utilizará el equipo.

.. raw:: pdf

    Spacer 0 12

**Pregunta 6 (A):** Los sistemas *GNU/Linux* son sistemas multitarea, ejecutan muchas tareas al mismo utilizando multiprogramación. Este tipo ejecución, que se denomina: 

A) *round robin*
B) *robin hood*
C) *flash round*

.. raw:: pdf

    Spacer 0 12

**Pregunta 7 (C):** Al utilizar el método de consola o terminal de texto en modo remoto... 

A) ...es necesario interrumpir al usuario que está realizando sus tareas.
B) ...se necesita apagar el entorno gráfico del equipo remoto.
C) ...se pueden realizar tareas de mantenimiento en servidores remotos.

.. raw:: pdf

    Spacer 0 12

**Pregunta 8 (B):** Los programas denominados **Shell Scripts** son...

A) ...archivos binarios que se ejecutan en el momento de inicio del sistema operativo.
B) ...archivos de texto con la secuencia de comandos a ejecutar definidos en el lenguaje específico del intérprete.
C) ...programas interactivos que necesitan la interacción del usuario antes de ejecutar cada comando.

.. raw:: pdf

    Spacer 0 12

**Pregunta 9 (C):** Los sistemas Unix en general, y por lo tanto también *GNU/Linux*, presentan una estructura de archivos...

A) ...específica que, de igual manera que otros sistemas operativos, utiliza letras para identificar los recursos de archivos.
B) ...secuencial, colocando los archivos uno a continuación del otro, identificados en un índice al final de la partición.
C) ...estándar que, a diferencia de otros sistemas operativos, considera a todos los componentes un archivo.

.. raw:: pdf

    Spacer 0 12

**Pregunta 10 (B):** El usuario que tiene todos los permisos sobre el sistema es denominado el **Super Usuario**, generalmente identificado como el usuario local:

A) *superuser*
B) *root*
C) *rootuser*
