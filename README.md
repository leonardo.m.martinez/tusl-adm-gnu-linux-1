# Administración de GNU/Linux I

Material de estudio de la materia **Administración de GNU/Linux I** de la carrera de grado [*Tecnicatura Universitaria en Software Libre*](http://www.unlvirtual.edu.ar/?portfolio=tecnicatura-en-software-libre) que se dicta en la *Universidad Nacional del Litoral* en la modalidad *a distancia*.


Todo el material está publicado con licencia [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](http://creativecommons.org/licenses/by-sa/4.0/) [![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

